'use strict';

// Create the 'moderador' controller
angular.module('moderador').controller('ModeradorController', ['$scope', '$location', 'Authentication', '$interval', '$timeout', 'Socket', 'SweetAlert', '$filter', '$document',
  function ($scope, $location, Authentication, $interval, $timeout, Socket, SweetAlert, $filter, $document) {
    var $translate = $filter('translate');
    $scope.contestInitial = true;
    $scope.nextButton = true;
    $scope.actualAnswer = 0;
    $scope.counterAnswer = 0;
    $scope.contestFinal = false;
    $scope.contestStarted = false;
    $scope.stats = false;
    $scope.contestCounter = true;
    $scope.contestEnd = false;
    $scope.text_Next = $translate('Next_Question');
    // $scope.contest_status = null;
    

    // If user is not signed in then redirect back home
    if (!Authentication.user) {
      $location.path('/');
    }

    // Make sure the Socket is connected
    // if (!Socket.socket) {
    //   Socket.connect();
    // }
    Socket.connect();

    // Almacena la info de la pregunta en concurso
    $scope.preguntaActual = {
      imageURL: 'modules/users/client/img/profile/default.png',
      time: 60,
      title: 'Siguiente pregunta',
      description: 'Aqui se muestra la descripción',
      answers: {
        correctas: 15,
        incorrectas: 5,
        abstenciones: 6
      }
    };

    // Almacena la info de la siguiente pregunta del concurso
    $scope.siguientePregunta = {
      imageURL: 'modules/users/client/img/profile/default.png',
      time: 60,
      title: 'Siguiente pregunta',
      description: 'Aqui se muestra la descripción',
    };

    ///Contador de tiempo por pregunta
    $scope.options = {
      skin: {
        type: 'tron',
        width: 5,
        color: '#494B52',
        spaceWidth: 3
      },
      unit: ' s',
      size: 200,
      barColor: '#494B52',
      trackWidth: 10,
      barWidth: 20,
      readOnly: true,
      textColor: '#494B52',
      step: 1,
      max: 100,
      dynamicOptions: true
    };

    //Estadisticas de usuarios
    $scope.pieChart = {
      options: {
        chart: {
          type: 'pie'
        },
        tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        }
      },
      title: {
        text: 'Estado de las respuestas de la pregunta.'
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          showInLegend: true
        }
      },
      series: [{
        name: 'Porciento',
        colorByPoint: true,
        data: [{
          name: 'Correctas',
          y: 0,
          sliced: true,
          color: '#009A00'
        }, {
          name: 'Incorrectas',
          y: 0,
          color: '#AA0000'
        }, {
          name: 'Sin respuesta',
          y: 100,
          color: '#00002A'
        }]
      }]
    };

    $scope.timeProm = {
      title: {
        text: 'Tiempos de respuesta.'
      },
      size: {
        height: 400
      },
      xAxis: {
        name: 'Preguntas',
        categories: []
      },
      series: [{
        name: 'Tiempo promedio en segundos.',
        data: []
      }]
    };

    // Grafico tipo barra para mostrar las estadisiticas
    $scope.barra = {
      options: {
        chart: {
          type: 'bar'
        },
        tooltip: {
          valueSuffix: ' votos'
        },
        plotOptions: {
          bar: {
            dataLabels: {
              enabled: true,
              style: {
                fontSize: '40px'
              }
            }
          }
        }
      },
      title: {
        text: 'Estado de las respuestas'
      },
      xAxis: {
        categories: ['Respuestas'],
        title: {
          text: null
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Valores',
          align: 'high'
        },
        labels: {
          overflow: 'justify'
        }
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true,
            style: {
              fontSize: '40px'
            }
          }
        }
      },
      legend: {
        layout: 'horizontal',
        align: 'right',
        verticalAlign: 'button',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: '#FFFFFF',
        shadow: true
      },
      credits: {
        enabled: false
      },
      series: [{
        name: 'Correctas',
        data: [0],
        color: '#009A00'
      }, {
        name: 'Incorrectas',
        data: [0],
        color: '#AA0000'
      }, {
        name: 'Sin respuesta',
        data: [0],
        color: '#00002A'
      }],
      size: {
        width: 650,
        height: 450
      }
    };

    // Grafico para los segundos
    $scope.kTimeQuestion = {
      displayPrevious: true,
      barCap: 25,
      trackWidth: 10,
      barWidth: 10,
      max: 60,
      trackColor: 'rgba(255,0,0,.1)',
      prevBarColor: 'rgba(0,0,0,.2)',
      textColor: 'rgba(255,0,0,.6)',
      size: 150,
      unit: ' s',
      readOnly: true,
      scale: {
        enabled: true,
        type: 'lines',
        color: 'gray',
        width: 1,
        quantity: 12,
        height: 8
      },
      dynamicOptions: true
    };

    // Grafico para los segundos
    $scope.kSeconds = {
      displayPrevious: true,
      barCap: 25,
      trackWidth: 10,
      barWidth: 10,
      max: 60,
      trackColor: 'rgba(255,0,0,.1)',
      prevBarColor: 'rgba(0,0,0,.2)',
      textColor: 'rgba(255,0,0,.6)',
      size: 150,
      unit: ' s',
      readOnly: true,
      scale: {
        enabled: true,
        type: 'lines',
        color: 'gray',
        width: 1,
        quantity: 12,
        height: 8
      }
    };

    // Grafico para los minutos
    $scope.kMinutes = {
      displayPrevious: true,
      barCap: 25,
      trackWidth: 10,
      barWidth: 10,
      max: 60,
      trackColor: 'rgba(255,0,0,.1)',
      prevBarColor: 'rgba(0,0,0,.2)',
      textColor: 'rgba(255,0,0,.6)',
      size: 150,
      unit: ' m',
      readOnly: true,
      scale: {
        enabled: true,
        type: 'lines',
        color: 'gray',
        width: 1,
        quantity: 12,
        height: 8
      }
    };

    // Grafico para las horas
    $scope.kHours = {
      displayPrevious: true,
      barCap: 25,
      trackWidth: 10,
      barWidth: 10,
      max: 60,
      trackColor: 'rgba(255,0,0,.1)',
      prevBarColor: 'rgba(0,0,0,.2)',
      textColor: 'rgba(255,0,0,.6)',
      size: 150,
      unit: ' h',
      readOnly: true,
      scale: {
        enabled: true,
        type: 'lines',
        color: 'gray',
        width: 1,
        quantity: 12,
        height: 8
      }
    };

    $scope.counter = function (t) {
      var segundosM = (t / 1000);
      if (t > 0) {
        $scope.seconds = Math.floor(segundosM % 60);
        $scope.minutes = Math.floor((segundosM / 60) % 60);
        $scope.hours = Math.floor((t / (3600000)) % 24);
        // $scope.days = Math.floor(t / (86400000));
      } else {
        $scope.seconds = 0;
        $scope.minutes = 0;
        // $scope.hours = 0;
        $scope.days = 0;
      }
    };

    ////////////////////////////////////////Acciones de los Botones ////////////////////////////////////////////////////


    // Emite una orden al servidor para comenzar el concurso
    $scope.startContest = function () {
      SweetAlert.swal({
        title: $translate('ARE_SURE'),
        text: $translate('START_CONTEST'),
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: $translate('SI'),
        cancelButtonText: $translate('NO'),
      },
      function (isConfirm) {
        if (isConfirm) {
          Socket.socket.emit('msg-start-question-server');
        }
      });
    };

    // Pasa la pregunta
    $scope.nextQuestion = function () {
      SweetAlert.swal({
        title: $translate('ARE_SURE_QUESTION'),
        text: $translate('START_QUESTION'),
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: $translate('SI'),
        cancelButtonText: $translate('NO'),
      },
      function (isConfirm) {
        if (isConfirm) {
          Socket.socket.emit('msg-start-question-server');
        }
      });
    };

    // Cambia el color del reloj segun el % de tiempo que resta de la pregunta
    $scope.changeColorTimer = function () {
      var percent = Math.round(($scope.actualTime * 100) / $scope.preguntaActual.time);
      if (percent >= 66) {
        $scope.kTimeQuestion = {
          barColor: 'rgb(85,172,85)',
        };
      } else if (percent >= 33 && percent < 66) {
        $scope.kTimeQuestion = {
          barColor: 'rgb(239,184,55)',
        };
      } else {
        $scope.kTimeQuestion = {
          barColor: 'rgba(255,0,0,.5)',
        };
      }
    };

    // Actualiza el tiempo restante de la pregunta actual
    $scope.updateQuestionInterval = function () {
      //var top = $scope.actualTime;
      $interval.cancel($scope.interval);
      $scope.interval = $interval(function () {
        $scope.actualTime--;
        $scope.changeColorTimer();
        // console.log($scope.actualTime);
        if ($scope.actualTime <= 0) {
          $scope.actualTime = 0;
          $interval.cancel($scope.interval);
        }

      }, 1000);
      $scope.$apply();
    };

    // Finaliza el concurso
    $scope.endContest = function () {
      SweetAlert.swal({
        title: $translate('End_Contest?'),
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: $translate('SI'),
        cancelButtonText: $translate('NO'),
      },
      function (isConfirm) {
        if (isConfirm) {
          Socket.emit('msg-end-contest');
        }
      });
    };

    // Envia un mensaje al servidor para mostrar el ganador del concurso
    $scope.showWinner = function () {
      SweetAlert.swal({
        title: $translate('Show_Winner?'),
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: $translate('SI'),
        cancelButtonText: $translate('NO'),
      },
      function (isConfirm) {
        if (isConfirm) {
          Socket.emit('msg-winner-contest');
        }
      });
    };

    // Envia un mensaje para finalizar la ronda de preguntas
    $scope.endQuestions = function() {
      SweetAlert.swal({
        title: $translate('Are_You_Sure_End_Questions'),
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: $translate('SI'),
        cancelButtonText: $translate('NO'),
      },
      function (isConfirm) {
        if (isConfirm) {
          Socket.emit('msg-end-questions');
        }
      });
    };
    
    //////                                                                                                                //////
    //////////////////////////////////////// ClientSockets Solaci - Moderador Operador  ////////////////////////////////////////
    //////                                                                                                                //////


    Socket.on('msg-update-timer-contest', function (message) {
      if ($scope.interval)
        $interval.cancel($scope.interval);

      $scope.count = 1000;
      $scope.interval = $interval(function () {
        $scope.count += 1000;
        $scope.counter(message.diferencia - $scope.count);
      }, 1000);
      $scope.contest_status = 'wait_start_contest';
    });

    // Para que no existan cambios en pantalla del tiempo
    // Socket.on('msg-update-timer-question', function (message) {
    //   // $scope.actualTime = Math.floor(message.diferencia / 1000);
    //   // $scope.updateQuestionInterval();
    //   // console.log('Actualizacion time');
    // });

    /*
     * Evento que espera a que comiencie el concurso
     * Ejecuta el contador hacia atras hasta que la hora de empezar el concurso llegue
     *
     **/

    Socket.on('msg-wait-start-contest', function (message) {
      $scope.contestInitial = true;
      $scope.stats = false;
      $scope.contestCounter = true;
      $scope.contestStarted = false;
      if ($scope.interval)
        $interval.cancel($scope.interval);

      $scope.count = 1000;
      $scope.interval = $interval(function () {
        $scope.count += 1000;
        $scope.counter(message.diferencia - $scope.count);
      }, 1000);
      $scope.contest_status = 'wait_start_contest';
      // console.log('Moderador: en espera que comience el concurso...');
    });

    /*
     * Evento que espera a que el moderador explique
     * Muestra la pantalla de inicio
     *
     **/
    Socket.on('msg-start-contest-moderador', function (message) {
      // message:
      // - imagen
      // - descripcion
      $interval.cancel($scope.interval);
      $scope.contestCounter = true;
      $scope.contestInitial = false;
      $scope.stats = true;

      for (var i = 1; i <= message.cantidad_preguntas; i++) {
        $scope.timeProm.xAxis.categories.push(i + '');
        $scope.timeProm.series[0].data.push(0);
      }
      $scope.contest_status = 'wait_start_contest';
      // console.log('Moderador: comienza el concurso...');
    });

    // Start Question
    Socket.on('msg-start-question-moderador', function (message) {
      // message:
      // - question
      // - next_question
      // console.log(message);
      $scope.contestCounter = false;
      $scope.finalizarRondaPreguntas = true;
      $scope.stats = true;
      $scope.preguntaActual = message.question;
      $scope.siguientePregunta = message.next_question;
      $scope.nextButton = true;
      if ($scope.siguientePregunta === null) {
        // $scope.text_Next = $translate('Questions_End');
      }
      $scope.actualTime = Math.floor(message.time / 1000);
      $scope.kTimeQuestion = {
        max: message.question.time
      };
      
      $scope.updateQuestionInterval();
      // Poniendo todo en 0 de la barra de stats para los usuarios
      $scope.barra.series = [{
        name: 'Correctas',
        data: [0],
        color: '#009A00'
      }, {
        name: 'Incorrectas',
        data: [0],
        color: '#AA0000'
      }, {
        name: 'Sin respuesta',
        data: [0],
        color: '#00002A'
      }];

      // Barra de stats para el moderador
      $scope.pieChart.series[0].data = [{
        name: 'Correctas',
        y: 0,
        sliced: true,
      }, {
        name: 'Incorrectas',
        y: 0,
      }, {
        name: 'Sin respuesta',
        y: 0
      }];
      // $scope.contest_status = 'start_question';
      $scope.contest_status = 'contest_active';

      console.log('Mostrando la pregunta');
      // Actualizo el progreso de la barra
      try {
        var progress = document.getElementById('progress');
        progress.style = 'width: ' + (($scope.preguntaActual.order/$scope.statistics.preguntas)*100) + '%';
      } catch(e) {

      }

    });

    // Start Moderator
    Socket.on('msg-wait-moderation-moderador', function (message) {
      // message:
      // - question
      // - next_question
      // console.log(message);
      $scope.rondaPreguntas = true;
      $scope.actualTime = 0;
      $scope.nextButton = ($scope.siguientePregunta === null);
      $scope.finalizarRondaPreguntas = false;
      $interval.cancel($scope.interval);
      $scope.contestStarted = true;
      $scope.stats = true;
      $scope.contestCounter = false;
      // if (message.opciones.length !== 0) {
        
      //   // $scope.contest_status = 'wait_moderation';
      // }
      $scope.contest_status = 'contest_active';
      console.log('Moderador: comienza de explicación de pregunta...');

    });

    /*
     *Evento que espera las estadisticas
     *
     */
    Socket.on('msg-update-statistics-question-moderador', function (message) {
      $scope.statistics = message;
      // console.log(message);
      if (message.opciones) {
        // Actualizo el grafico con la cantidad de respuestas recibidas por opcion
        $scope.barra.series = [];
        var colors = new Array('#397EBF', '#C38034', '#D92EBD', '#F3EF3F', '#30CED1');
        angular.forEach(message.opciones, function (value, key) {
          $scope.barra.series.push({
            name: value.name,
            data: [value.count],
            color: colors[key]
          });
        });

        // if (message.opciones[0].correct) {

        // }
        // $scope.barra.series[0].data = [52];
        // $scope.barra.series[0].name = message.opciones[0].;

        // $scope.barra.series[1].data = [20];
        // $scope.barra.series[1].name = 'Pepito 2';

        // $scope.barra.series[2].data = [12];
        // $scope.barra.series[2].name = 'Sin respuesta';
        // $scope.barra.series[2].color = '#AA0000';
      } else {
        // Actualizo en el grafico las stats de la pregunta actual
        // Actualizando la barra con los valores recibidos
        // console.log($scope.barra.series);
        $scope.barra.series[0].data = [message.respuestas_correctas];
        $scope.barra.series[1].data = [message.respuestas_incorrectas];
        $scope.barra.series[2].data = [message.abstenciones];
        
        //Pintando el pastel de los usuarios
        $scope.pieChart.series[0].data = [{
          name: 'Correctas',
          y: message.respuestas_correctas,
          sliced: true,
        }, {
          name: 'Incorrectas',
          y: message.respuestas_incorrectas,
        }, {
          name: 'Sin respuesta',
          y: message.abstenciones
        }];
      } 
      $scope.contest_status = 'contest_active';
      //Actualizando la grafica de los tiempos de respuesta
      if ($scope.actualAnswer !== message.question_id) {
        $scope.counterAnswer++;
      }
      $scope.timeProm.series[0].data[$scope.counterAnswer] = (Math.floor(message.tiempo_promedio / 1000));
      // console.log($scope.timeProm.xAxis.categories);
      $scope.actualAnswer = message.question_id;

      console.log('Moderador Estadístico: mostrar estadísticas de la pregunta...');
    });


    // End Questions
    Socket.on('msg-end-questions-moderador', function (message) {
      $scope.statistics = message;
      // $scope.rondaPreguntas = false;
      $scope.stats = false;
      $scope.contestCounter = false;
      $scope.winner = message.user_win.displayName;
      $scope.contest_status = 'end_questions';
      // console.log(message);
      console.log('...se agotaron las preguntas del concurso...');
    });

    // Se muetra el ganador del concurso a todas las personas
    Socket.on('msg-winner-contest-presentacion', function (message) {
      $scope.statistics = message;
      $scope.contestStarted = false;
      $scope.contestInitial = false;
      $scope.stats = false;
      $scope.contestCounter = false;
      $scope.contestFinal = true;
      $scope.contest_winner = {
        user: message.user,
        correctas: message.respuestas_correctas,
        incorrectas: message.respuestas_incorrectas,
        abstenciones: message.abstenciones,
        tiempo_total: ((message.tiempo_promedio*message.preguntas)/1000).toFixed(3),
        tiempo_promedio: (message.tiempo_promedio/1000).toFixed(3)
      };
      $scope.contest_status = 'winner_contest';
      console.log('Moderador Operador: mostrar pantalla al ganador, felicitación');
    });

    // El concurso ha finalizado
    Socket.on('msg-end-contest-moderador', function (message) {
      $scope.contest_status = 'end_contest';
      console.log('Moderador Operador: mostrar pantalla estadísticas generales y al ganador');
      // Eenviar emit al servidor para el ganador
    });

    // Actualiza la cantidad de usuarios 
    Socket.on('msg-update-users-contest', function (message) {
      // console.log(message);
      $scope.cant_usuarios_registrados = message.total_registrados;
      $scope.cant_usuarios_conectados = message.total_conectados;
      // console.log('Moderador Operador: mostrar pantalla estadísticas generales y al ganador');
    });
  }
]);
