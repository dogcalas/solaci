'use strict';

// Create the 'moderador' controller
angular.module('moderador').controller('PresentacionController', ['$scope', '$location', 'Authentication', '$timeout', 'Socket', '$interval', '$element', '$document',
  function ($scope, $location, Authentication, $timeout, Socket, $interval, $element, $document) {
    // If user is not signed in then redirect back home
    if (!Authentication.user) {
      $location.path('/');
    }

    // Make sure the Socket is connected
    // if (!Socket.socket) {
    //   Socket.connect();
    // }
    Socket.connect();
    // $scope.contest_status = null;
    $scope.config = {}; // use defaults
    $scope.model = {}; // always pass empty object
    $scope.zoom = {
      zoomLvl: 5.3
    };
    // Activa la imagen de la pregunta actual a pantalla completa
    $scope.fullScreen = function() {
      var img = document.getElementById('imagenConcurso');
      if (img.requestFullscreen) {
        img.requestFullscreen();
      } else if (img.msRequestFullscreen) {
        img.msRequestFullscreen();
      } else if (img.mozRequestFullScreen) {
        img.mozRequestFullScreen();
      } else if (img.webkitRequestFullscreen) {
        img.webkitRequestFullscreen();
      }
    };

    // Funcion para calcular el timpo restante para empezar el concurso
    $scope.countDownContest = function(t) {
      var segundos = (t/1000);
      if(t>0) {
        $scope.seconds = Math.floor(segundos % 60);
        $scope.minutes = Math.floor((segundos/60) % 60);
        $scope.hours = Math.floor((t/(3600000)) % 24);
        // $scope.days = Math.floor(t/(86400000));
      } else {
        $scope.seconds = 0;
        $scope.minutes = 0;
        $scope.hours = 0;
        // $scope.days = 0;
      }
    };

    // Grafico para los segundos
    $scope.kSeconds = {
      displayPrevious: true,
      barCap: 25,
      trackWidth: 10,
      barWidth: 10,
      max: 60,
      trackColor: 'rgba(255,0,0,.1)',
      prevBarColor: 'rgba(0,0,0,.2)',
      textColor: 'rgba(255,0,0,.6)',
      size: 150,
      unit: ' s',
      readOnly: true,
      scale: {
        enabled: true,
        type: 'lines',
        color: 'gray',
        width: 1,
        quantity: 12,
        height: 8
      }
    };

    // Grafico para los minutos
    $scope.kMinutes = {
      displayPrevious: true,
      barCap: 25,
      trackWidth: 10,
      barWidth: 10,
      max: 60,
      trackColor: 'rgba(255,0,0,.1)',
      prevBarColor: 'rgba(0,0,0,.2)',
      textColor: 'rgba(255,0,0,.6)',
      size: 150,
      unit: ' m',
      readOnly: true,
      scale: {
        enabled: true,
        type: 'lines',
        color: 'gray',
        width: 1,
        quantity: 12,
        height: 8
      }
    };

    // Grafico para las horas
    $scope.kHours = {
      displayPrevious: true,
      barCap: 25,
      trackWidth: 10,
      barWidth: 10,
      max: 60,
      trackColor: 'rgba(255,0,0,.1)',
      prevBarColor: 'rgba(0,0,0,.2)',
      textColor: 'rgba(255,0,0,.6)',
      size: 150,
      unit: ' h',
      readOnly: true,
      scale: {
        enabled: true,
        type: 'lines',
        color: 'gray',
        width: 1,
        quantity: 12,
        height: 8
      }
    }; 

    //////
    // ClientSockets Solaci - Moderador Presentación
    //////

    var random = function randomIntInc (low, high) {
      return Math.floor(Math.random() * (high - low + 1) + low);
    };

    // Timers || Actualiza el tiempo de la pregunta actual
    Socket.on('msg-update-timer-contest', function (message) {
      if($scope.interval)
        $interval.cancel($scope.interval);
      $scope.count = 1000;
      $scope.interval = $interval(function(){
        $scope.count +=1000;
        $scope.countDownContest(message.diferencia - $scope.count);
      },1000);
      $scope.contest_status = 'wait';
      // console.log('Actualizando el tiempo del concurso');
    });

    // Wait Contest || Actualiza el tiempo para empezar el concurso
    Socket.on('msg-wait-start-contest', function (message) {
      if($scope.interval)
        $interval.cancel($scope.interval);
      $scope.count = 1000;
      $scope.interval = $interval(function(){
        $scope.count +=1000;
        $scope.countDownContest(message.diferencia - $scope.count);
      },1000);
      // Pongo el estado del concurso en espera y se muestra el contador regresivo
      $scope.contest_status = 'wait';
      // console.log('Se espera por comienzo del concurso...');
    });

    // Start Contest || El concurso ha iniciado y el concursante debe prestar atencion al moredador
    Socket.on('msg-start-contest-moderador', function (message) {
      // Cambio el estado del concurso para decirle al usuario que debe presetar atencion al moderador
      $scope.contest_status = 'start_contest';
      // console.log('Moderador Presentador: mostrar pantalla de presentación del concurso a los concursantes');
    });

    // Start Question || Empieza una pregunta
    Socket.on('msg-start-question-moderador', function (message) {
      // Aqui se actualiza la URL de la imagen en concurso y se muestra a pantalla completa
      $scope.imageURL = message.question.imageURL;
      $scope.contest_status = 'start_question';
      // var img = document.getElementById('imagenConcurso');
      // img.requestFullscreen();
      $scope.fullScreen();
      console.log('Moderador Presentador: mostrar imagen de la pregunta a los concursantes');
    });

    // Start Moderator
    Socket.on('msg-wait-moderation-moderador', function (message) {
      // La imagen vuelve a su estado original y se da la posibilidad de hacer zoom
      // if (document.exitFullscreen) {
      //   document.exitFullscreen();
      // } else if (document.msExitFullscreen) {
      //   document.msExitFullscreen();
      // } else if (document.mozCancelFullScreen) {
      //   document.mozCancelFullScreen();
      // } else if (document.webkitExitFullscreen) {
      //   document.webkitExitFullscreen();
      // }
      // $scope.contest_status = 'wait_moderation';
      // console.log('Moderador Presentador: mantiene la imagen de la pregunta a los concursantes');
    });
    
    // End Questions || Hay que mostrar un cartel de agradecimiento y esperando ganador
    Socket.on('msg-end-questions-moderador', function (message) {
      $scope.contest_status = 'end_questions';
      // console.log('...se agotaron las preguntas del concurso...');
    });

    // El concurso ha finalizado
    Socket.on('msg-end-contest-moderador', function (message) {
      $scope.contest_status = 'end_contest';
      // console.log('Moderador Presentador: mostrar pantalla en espera del ganador, contador decremento');
    });

    // Se muetra el ganador del concurso a todas las personas
    Socket.on('msg-winner-contest-presentacion', function (message) {
      $scope.contest_winner = {
        user: message.user,
        correctas: message.respuestas_correctas,
        incorrectas: message.respuestas_incorrectas,
        abstenciones: message.abstenciones,
        tiempo_total: ((message.tiempo_promedio*message.preguntas)/1000).toFixed(3),
        tiempo_promedio: (message.tiempo_promedio/1000).toFixed(3)
      };
      $scope.contest_status = 'winner_contest';
      // console.log('Moderador Presentador: mostrar pantalla al ganador más estadísticas');
    });

    // Remove the event listener when the controller instance is destroyed
    $scope.$on('$destroy', function () {
      Socket.removeListener('msg-winner-contest-presentacion');
      Socket.removeListener('msg-wait-moderation-moderador');
    });
  }
]);
