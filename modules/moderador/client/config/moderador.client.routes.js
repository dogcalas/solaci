'use strict';

// Configure the 'moderador' module routes
angular.module('moderador').config(['$stateProvider',
  function ($stateProvider) {
    $stateProvider
      .state('moderador', {
        url: '/moderador',
        templateUrl: 'modules/moderador/client/views/moderador.client.view.html',
        data: {
          roles: ['moderador']
        }
      })
      .state('estadistica', {
        url: '/estadistica',
        templateUrl: 'modules/moderador/client/views/estadistica.client.view.html',
        data: {
          roles: ['moderador']
        },
        controller: 'ModeradorController'
      })
      .state('presentacion', {
        url: '/presentacion',
        // resolve: {
        //   myR: function($http) {
        //     return $http.get("/api/presentacion/");
        //   }
        // },
        templateUrl: 'modules/moderador/client/views/presentacion.client.view.html',
        data: {
          roles: ['moderador']
        }
      });
  }
]);
