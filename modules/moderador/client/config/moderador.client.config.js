'use strict';

// Configuring the Moderador module
angular.module('moderador').run(['Menus',
  function (Menus) {
    // Set top bar menu items
    Menus.addMenuItem('topbar', {
      title: 'Moderador',
      state: 'moderador',
      roles: ['moderador']
    });
    // Estadistica
    Menus.addMenuItem('topbar', {
      title: 'Estadística',
      state: 'estadistica',
      roles: ['moderador']
    });
    // Presentacion
    // Menus.addMenuItem('topbar', {
    //   title: 'Presentación',
    //   state: 'presentacion',
    //   roles: ['moderador']
    // });
  }
]);
