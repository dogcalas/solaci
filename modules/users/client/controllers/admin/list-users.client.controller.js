'use strict';

angular.module('users.admin').controller('UserListController', ['$scope', '$state', '$filter', 'Admin', '$translate', 'localStorageService', 'Socket',
  function ($scope, $state, $filter, Admin, $translate, localStorageService, Socket) {
    // $scope.user = userResolve;
    var value = localStorageService.get('idioma_active');
    if (value !== null) {
      $translate.use(value);
    } else {
      localStorageService.add('idioma_active', 'es');
    }

    $scope.changeLanguage = function(key) {
      localStorageService.add('idioma_active', key);
      $translate.use(key);
    };

    Admin.query(function (data) {
      $scope.users = data;
      $scope.buildPager();
    });

    $scope.buildPager = function () {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 25;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };

    $scope.figureOutItemsToDisplay = function () {
      $scope.filteredItems = $filter('filter')($scope.users, {
        $: $scope.search
      });
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };

    $scope.pageChanged = function () {
      $scope.figureOutItemsToDisplay();
    };

    $scope.esConcursante = function(roles) {
      // Si el rol concursante no peretenece al usuario retorno false (no es concursante) sino true
      if (roles.lastIndexOf('concursante') === -1) {
        return false;
      } else {
        return true;
      }
    };

    $scope.convertirEnConcursante = function(user) {
      user.roles.push('concursante');
      user.$update(function () {
        // recargarUsuarios();
        Socket.emit('msg-activated-user', {
          uid: user._id
        });
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    $scope.remove = function(user) {
      if (confirm('Are you sure you want to delete to ' + user.username + '?')) {
        if (user) {
          user.$remove(function () {
            recargarUsuarios();
          });
          // $scope.users.splice($scope.users.indexOf(user), 1);
          // $state.go('admin.users');

        } else {
          $scope.user.$remove(function () {
            $state.go('admin.users');
          });
        }
      }
    };

    function recargarUsuarios() {
      Admin.query(function (data) {
        $scope.users = data;
        $scope.buildPager();
      });
    }
  }
]);
