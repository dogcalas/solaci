'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$state', '$http', '$location', '$window', 'Authentication', '$translate', 'localStorageService',
  function ($scope, $state, $http, $location, $window, Authentication, $translate, localStorageService) {
    $scope.authentication = Authentication;
    // $scope.popoverMsg = PasswordValidator.getPopoverMsg();

    var value = localStorageService.get('idioma_active');
    if (value !== null) {
      $translate.use(value);
    } else {
      localStorageService.add('idioma_active', 'es');
    }

    $scope.changeLanguage = function(key) {
      localStorageService.add('idioma_active', key);
      $translate.use(key);
    };

    // Get an eventual error defined in the URL query string:
    $scope.error = $location.search().err;

    // If user is signed in then redirect back home
    if ($scope.authentication.user) {
      // Busco si el usuario es concursante para redireccionarlo al concurso sino lo llevo a otro estado
      if ($scope.authentication.user.roles.indexOf('concursante') !== -1)
        $state.go('concurso', {}, { reload: true, inherit: false, notify: true });
      // else
      //   $location.path('/');
    }

    $scope.countries = [ // Taken from https://gist.github.com/unceus/6501985
      { name: 'Afghanistan', code: 'AF', lang: 'es' },
      { name: 'Åly Islys', code: 'AX', lang: 'es' },
      { name: 'Albania', code: 'AL', lang: 'es' },
      { name: 'Alemania', code: 'DE', lang: 'es' },
      { name: 'Algeria', code: 'DZ', lang: 'es' },
      { name: 'American Samoa', code: 'AS', lang: 'es' },
      { name: 'Andorra', code: 'AD', lang: 'es' },
      { name: 'Angola', code: 'AO', lang: 'es' },
      { name: 'Anguilla', code: 'AI', lang: 'es' },
      { name: 'Antarctica', code: 'AQ', lang: 'es' },
      { name: 'Antigua y Barbuda', code: 'AG', lang: 'es' },
      { name: 'Argentina', code: 'AR', lang: 'es' },
      { name: 'Armenia', code: 'AM', lang: 'es' },
      { name: 'Aruba', code: 'AW', lang: 'es' },
      { name: 'Australia', code: 'AU', lang: 'es' },
      { name: 'Austria', code: 'AT', lang: 'es' },
      { name: 'Azerbaijan', code: 'AZ', lang: 'es' },
      { name: 'Bahamas', code: 'BS', lang: 'es' },
      { name: 'Bahrain', code: 'BH', lang: 'es' },
      { name: 'Bangladesh', code: 'BD', lang: 'es' },
      { name: 'Barbados', code: 'BB', lang: 'es' },
      { name: 'Belarus', code: 'BY', lang: 'es' },
      { name: 'Belgium', code: 'BE', lang: 'es' },
      { name: 'Belize', code: 'BZ', lang: 'es' },
      { name: 'Benin', code: 'BJ', lang: 'es' },
      { name: 'Bermuda', code: 'BM', lang: 'es' },
      { name: 'Bhutan', code: 'BT', lang: 'es' },
      { name: 'Bolivia', code: 'BO', lang: 'es' },
      { name: 'Bosnia y Herzegovina', code: 'BA', lang: 'es' },
      { name: 'Botswana', code: 'BW', lang: 'es' },
      { name: 'Bouvet Isly', code: 'BV', lang: 'es' },
      { name: 'Brasil', code: 'BR', lang: 'es' },
      { name: 'British Indian Ocean Territory', code: 'IO', lang: 'es' },
      { name: 'Brunei Darussalam', code: 'BN', lang: 'es' },
      { name: 'Bulgaria', code: 'BG', lang: 'es' },
      { name: 'Burkina Faso', code: 'BF', lang: 'es' },
      { name: 'Burundi', code: 'BI', lang: 'es' },
      { name: 'Cambodia', code: 'KH', lang: 'es' },
      { name: 'Cameroon', code: 'CM', lang: 'es' },
      { name: 'Canada', code: 'CA', lang: 'es' },
      { name: 'Cape Verde', code: 'CV', lang: 'es' },
      { name: 'Cayman Islys', code: 'KY', lang: 'es' },
      { name: 'República Central Africana', code: 'CF', lang: 'es' },
      { name: 'Chad', code: 'TD', lang: 'es' },
      { name: 'Chile', code: 'CL', lang: 'es' },
      { name: 'China', code: 'CN', lang: 'es' },
      { name: 'Christmas Isly', code: 'CX', lang: 'es' },
      { name: 'Cocos (Keeling) Islys', code: 'CC', lang: 'es' },
      { name: 'Colombia', code: 'CO', lang: 'es' },
      { name: 'Comoros', code: 'KM', lang: 'es' },
      { name: 'Congo', code: 'CG', lang: 'es' },
      { name: 'República Democrática del Congo', code: 'CD', lang: 'es' },
      { name: 'Cook Islys', code: 'CK', lang: 'es' },
      { name: 'Costa Rica', code: 'CR', lang: 'es' },
      { name: 'Cote D\'Ivoire', code: 'CI', lang: 'es' },
      { name: 'Croatia', code: 'HR', lang: 'es' },
      { name: 'Cuba', code: 'CU', lang: 'es' },
      { name: 'Cyprus', code: 'CY', lang: 'es' },
      { name: 'República Cheka', code: 'CZ', lang: 'es' },
      { name: 'Denmark', code: 'DK', lang: 'es' },
      { name: 'Djibouti', code: 'DJ', lang: 'es' },
      { name: 'Dominica', code: 'DM', lang: 'es' },
      { name: 'República Dominicana', code: 'DO', lang: 'es' },
      { name: 'Ecuador', code: 'EC', lang: 'es' },
      { name: 'Egipto', code: 'EG', lang: 'es' },
      { name: 'El Salvador', code: 'SV', lang: 'es' },
      { name: 'Equatorial Guinea', code: 'GQ', lang: 'es' },
      { name: 'Eritrea', code: 'ER', lang: 'es' },
      { name: 'Estonia', code: 'EE', lang: 'es' },
      { name: 'España', code: 'ES', lang: 'es' },
      { name: 'Ethiopia', code: 'ET', lang: 'es' },
      { name: 'Falkly Islys (Malvinas)', code: 'FK', lang: 'es' },
      { name: 'Faroe Islys', code: 'FO', lang: 'es' },
      { name: 'Fiji', code: 'FJ', lang: 'es' },
      { name: 'Finly', code: 'FI', lang: 'es' },
      { name: 'France', code: 'FR', lang: 'es' },
      { name: 'French Guiana', code: 'GF', lang: 'es' },
      { name: 'French Polynesia', code: 'PF', lang: 'es' },
      { name: 'French Southern Territories', code: 'TF', lang: 'es' },
      { name: 'Gabon', code: 'GA', lang: 'es' },
      { name: 'Gambia', code: 'GM', lang: 'es' },
      { name: 'Georgia', code: 'GE', lang: 'es' },
      { name: 'Ghana', code: 'GH', lang: 'es' },
      { name: 'Gibraltar', code: 'GI', lang: 'es' },
      { name: 'Grecia', code: 'GR', lang: 'es' },
      { name: 'Greenly', code: 'GL', lang: 'es' },
      { name: 'Grenada', code: 'GD', lang: 'es' },
      { name: 'Guadeloupe', code: 'GP', lang: 'es' },
      { name: 'Guam', code: 'GU', lang: 'es' },
      { name: 'Guatemala', code: 'GT', lang: 'es' },
      { name: 'Guernsey', code: 'GG', lang: 'es' },
      { name: 'Guinea', code: 'GN', lang: 'es' },
      { name: 'Guinea-Bissau', code: 'GW', lang: 'es' },
      { name: 'Guyana', code: 'GY', lang: 'es' },
      { name: 'Haití', code: 'HT', lang: 'es' },
      { name: 'Heard Isly y Mcdonald Islys', code: 'HM', lang: 'es' },
      { name: 'Holy See (Vatican City State)', code: 'VA', lang: 'es' },
      { name: 'Honduras', code: 'HN', lang: 'es' },
      { name: 'Hong Kong', code: 'HK', lang: 'es' },
      { name: 'Hungary', code: 'HU', lang: 'es' },
      { name: 'Icely', code: 'IS', lang: 'es' },
      { name: 'India', code: 'IN', lang: 'es' },
      { name: 'Indonesia', code: 'ID', lang: 'es' },
      { name: 'República Islámica de Iran', code: 'IR', lang: 'es' },
      { name: 'Iraq', code: 'IQ', lang: 'es' },
      { name: 'Irely', code: 'IE', lang: 'es' },
      { name: 'Isle of Man', code: 'IM', lang: 'es' },
      { name: 'Israel', code: 'IL', lang: 'es' },
      { name: 'Italia', code: 'IT', lang: 'es' },
      { name: 'Jamaica', code: 'JM', lang: 'es' },
      { name: 'Japón', code: 'JP', lang: 'es' },
      { name: 'Jersey', code: 'JE', lang: 'es' },
      { name: 'Jordan', code: 'JO', lang: 'es' },
      { name: 'Kazakhstan', code: 'KZ', lang: 'es' },
      { name: 'Kenya', code: 'KE', lang: 'es' },
      { name: 'Kiribati', code: 'KI', lang: 'es' },
      { name: 'República Democrática de Corea', code: 'KP', lang: 'es' },
      { name: 'República de Corea', code: 'KR', lang: 'es' },
      { name: 'Kuwait', code: 'KW', lang: 'es' },
      { name: 'Kyrgyzstan', code: 'KG', lang: 'es' },
      { name: 'Lao People\'s Democratic República', code: 'LA', lang: 'es' },
      { name: 'Latvia', code: 'LV', lang: 'es' },
      { name: 'Lebanon', code: 'LB', lang: 'es' },
      { name: 'Lesotho', code: 'LS', lang: 'es' },
      { name: 'Liberia', code: 'LR', lang: 'es' },
      { name: 'Libyan Arab Jamahiriya', code: 'LY', lang: 'es' },
      { name: 'Liechtenstein', code: 'LI', lang: 'es' },
      { name: 'Lithuania', code: 'LT', lang: 'es' },
      { name: 'Luxembourg', code: 'LU', lang: 'es' },
      { name: 'Macao', code: 'MO', lang: 'es' },
      { name: 'Macedonia, The Former Yugoslav República of', code: 'MK', lang: 'es' },
      { name: 'Madagascar', code: 'MG', lang: 'es' },
      { name: 'Malawi', code: 'MW', lang: 'es' },
      { name: 'Malaysia', code: 'MY', lang: 'es' },
      { name: 'Maldives', code: 'MV', lang: 'es' },
      { name: 'Mali', code: 'ML', lang: 'es' },
      { name: 'Malta', code: 'MT', lang: 'es' },
      { name: 'Marshall Islys', code: 'MH', lang: 'es' },
      { name: 'Martinique', code: 'MQ', lang: 'es' },
      { name: 'Mauritania', code: 'MR', lang: 'es' },
      { name: 'Mauritius', code: 'MU', lang: 'es' },
      { name: 'Mayotte', code: 'YT', lang: 'es' },
      { name: 'México', code: 'MX', lang: 'es' },
      { name: 'Micronesia, Federated States of', code: 'FM', lang: 'es' },
      { name: 'Moldova, República of', code: 'MD', lang: 'es' },
      { name: 'Mónaco', code: 'MC', lang: 'es' },
      { name: 'Mongolia', code: 'MN', lang: 'es' },
      { name: 'Montserrat', code: 'MS', lang: 'es' },
      { name: 'Morocco', code: 'MA', lang: 'es' },
      { name: 'Mozambique', code: 'MZ', lang: 'es' },
      { name: 'Myanmar', code: 'MM', lang: 'es' },
      { name: 'Namibia', code: 'NA', lang: 'es' },
      { name: 'Nauru', code: 'NR', lang: 'es' },
      { name: 'Nepal', code: 'NP', lang: 'es' },
      { name: 'Netherlys', code: 'NL', lang: 'es' },
      { name: 'Netherlys Antilles', code: 'AN', lang: 'es' },
      { name: 'Nueva Caledonia', code: 'NC', lang: 'es' },
      { name: 'Nueva Zealy', code: 'NZ', lang: 'es' },
      { name: 'Nicaragua', code: 'NI', lang: 'es' },
      { name: 'Niger', code: 'NE', lang: 'es' },
      { name: 'Nigeria', code: 'NG', lang: 'es' },
      { name: 'Niue', code: 'NU', lang: 'es' },
      { name: 'Norfolk Isly', code: 'NF', lang: 'es' },
      { name: 'Northern Mariana Islys', code: 'MP', lang: 'es' },
      { name: 'Norway', code: 'NO', lang: 'es' },
      { name: 'Oman', code: 'OM', lang: 'es' },
      { name: 'Pakistan', code: 'PK', lang: 'es' },
      { name: 'Palau', code: 'PW', lang: 'es' },
      { name: 'Palestinian Territory, Occupied', code: 'PS', lang: 'es' },
      { name: 'Panama', code: 'PA', lang: 'es' },
      { name: 'Papua Nueva Guinea', code: 'PG', lang: 'es' },
      { name: 'Paraguay', code: 'PY', lang: 'es' },
      { name: 'Peru', code: 'PE', lang: 'es' },
      { name: 'Philippines', code: 'PH', lang: 'es' },
      { name: 'Pitcairn', code: 'PN', lang: 'es' },
      { name: 'Poly', code: 'PL', lang: 'es' },
      { name: 'Portugal', code: 'PT', lang: 'es' },
      { name: 'Puerto Rico', code: 'PR', lang: 'es' },
      { name: 'Qatar', code: 'QA', lang: 'es' },
      { name: 'Reunion', code: 'RE', lang: 'es' },
      { name: 'Romania', code: 'RO', lang: 'es' },
      { name: 'Russian Federation', code: 'RU', lang: 'es' },
      { name: 'Rwya', code: 'RW', lang: 'es' },
      { name: 'Saint Helena', code: 'SH', lang: 'es' },
      { name: 'Saint Kitts y Nevis', code: 'KN', lang: 'es' },
      { name: 'Saint Lucia', code: 'LC', lang: 'es' },
      { name: 'Saint Pierre y Miquelon', code: 'PM', lang: 'es' },
      { name: 'Saint Vincent y the Grenadines', code: 'VC', lang: 'es' },
      { name: 'Samoa', code: 'WS', lang: 'es' },
      { name: 'San Marino', code: 'SM', lang: 'es' },
      { name: 'Sao Tome y Principe', code: 'ST', lang: 'es' },
      { name: 'Saudi Arabia', code: 'SA', lang: 'es' },
      { name: 'Senegal', code: 'SN', lang: 'es' },
      { name: 'Serbia y Montenegro', code: 'CS', lang: 'es' },
      { name: 'Seychelles', code: 'SC', lang: 'es' },
      { name: 'Sierra Leone', code: 'SL', lang: 'es' },
      { name: 'Singapore', code: 'SG', lang: 'es' },
      { name: 'Slovakia', code: 'SK', lang: 'es' },
      { name: 'Slovenia', code: 'SI', lang: 'es' },
      { name: 'Solomon Islys', code: 'SB', lang: 'es' },
      { name: 'Somalia', code: 'SO', lang: 'es' },
      { name: 'South Africa', code: 'ZA', lang: 'es' },
      { name: 'South Georgia y the South Sywich Islys', code: 'GS', lang: 'es' },
      { name: 'Sri Lanka', code: 'LK', lang: 'es' },
      { name: 'Sudan', code: 'SD', lang: 'es' },
      { name: 'Suriname', code: 'SR', lang: 'es' },
      { name: 'Svalbard y Jan Mayen', code: 'SJ', lang: 'es' },
      { name: 'Swazily', code: 'SZ', lang: 'es' },
      { name: 'Sweden', code: 'SE', lang: 'es' },
      { name: 'Switzerly', code: 'CH', lang: 'es' },
      { name: 'Syrian Arab República', code: 'SY', lang: 'es' },
      { name: 'Taiwan, Province of China', code: 'TW', lang: 'es' },
      { name: 'Tajikistan', code: 'TJ', lang: 'es' },
      { name: 'Tanzania, United República of', code: 'TZ', lang: 'es' },
      { name: 'Thaily', code: 'TH', lang: 'es' },
      { name: 'Timor-Leste', code: 'TL', lang: 'es' },
      { name: 'Togo', code: 'TG', lang: 'es' },
      { name: 'Tokelau', code: 'TK', lang: 'es' },
      { name: 'Tonga', code: 'TO', lang: 'es' },
      { name: 'Trinidad y Tobago', code: 'TT', lang: 'es' },
      { name: 'Tunisia', code: 'TN', lang: 'es' },
      { name: 'Turkey', code: 'TR', lang: 'es' },
      { name: 'Turkmenistan', code: 'TM', lang: 'es' },
      { name: 'Turks y Caicos Islys', code: 'TC', lang: 'es' },
      { name: 'Tuvalu', code: 'TV', lang: 'es' },
      { name: 'Ugya', code: 'UG', lang: 'es' },
      { name: 'Ucrania', code: 'UA', lang: 'es' },
      { name: 'Emiratos Árabes Unidos', code: 'AE', lang: 'es' },
      { name: 'Reino Unido', code: 'GB', lang: 'es' },
      { name: 'Estados Unidos', code: 'US', lang: 'es' },
      { name: 'United States Minor Outlying Islys', code: 'UM', lang: 'es' },
      { name: 'Uruguay', code: 'UY', lang: 'es' },
      { name: 'Uzbekistan', code: 'UZ', lang: 'es' },
      { name: 'Vanuatu', code: 'VU', lang: 'es' },
      { name: 'Venezuela', code: 'VE', lang: 'es' },
      { name: 'Vietnam', code: 'VN', lang: 'es' },
      { name: 'Virgin Islys, British', code: 'VG', lang: 'es' },
      { name: 'Virgin Islys, U.S.', code: 'VI', lang: 'es' },
      { name: 'Wallis y Futuna', code: 'WF', lang: 'es' },
      { name: 'Western Sahara', code: 'EH', lang: 'es' },
      { name: 'Yemen', code: 'YE', lang: 'es' },
      { name: 'Zambia', code: 'ZM', lang: 'es' },
      { name: 'Zimbabwe', code: 'ZW', lang: 'es' }
    ];
    $scope.signup = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'userForm');

        return false;
      }
      $scope.userForm.$setUntouched();
      $scope.credentials.password = $scope.credentials.username;
      $http.post('/api/auth/signup', $scope.credentials).success(function (response) {

        // SweetAlert.swal("Good job!", "You clicked the button!", "success");

         // If successful we assign the response to the global user model
        // $scope.authentication.user = response;

        // And redirect to the previous or home page
        // Busco si el usuario es concursante para redireccionarlo al concurso sino lo llevo a otro estado
        // if ($scope.authentication.user.roles.indexOf('concursante') !== -1) {
        //   $state.go('concurso', {}, { reload: true, inherit: false, notify: true });
        // } else {
          $state.go($state.previous.state.name || 'home', $state.previous.params);
        // }

      }).error(function (response) {
        $scope.error = response.message;
      });
    };

    $scope.signin = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'userForm');
        return false;
      }

      $scope.credentials.username = $scope.credentials.password;
      $http.post('/api/auth/signin', $scope.credentials).success(function (response) {
        // If successful we assign the response to the global user model
        $scope.authentication.user = response;
        // Make sure the Socket is connected
        // if (!Socket.socket) {
        //   Socket.connect();
        // }

        // And redirect to the previous or home page
        // Busco si el usuario es concursante para redireccionarlo al concurso sino lo llevo a otro estado
        if ($scope.authentication.user.roles.indexOf('concursante') !== -1) {
          $state.go('concurso');
        } else if ($scope.authentication.user.roles.indexOf('moderador') !== -1) {
          $state.go('moderador');
        } else if ($scope.authentication.user.roles.indexOf('admin') !== -1) {
          $state.go('admin.users');
        } else {
          $state.go($state.previous.state.name || 'home', $state.previous.params);
        }
        // $state.go($state.previous.state.name || 'home', $state.previous.params);
      }).error(function (response) {
        $scope.error = response.message;
      });
    };

    // OAuth provider request
    $scope.callOauthProvider = function (url) {
      if ($state.previous && $state.previous.href) {
        url += '?redirect_to=' + encodeURIComponent($state.previous.href);
      }

      // Effectively call OAuth authentication route:
      $window.location.href = url;
    };

    // Make sure the Socket is connected
    // if (!Socket.socket) {
    //   Socket.connect();
    // }

    // $scope.contries = [{
    //   'dom': 'AF',
    //   'name': 'Afghanistan'
    // }];

    // $scope.showContries = function(country) {
    //   var temp = $('.ui-select-container .btn-primary').text().split(' ');
    //   temp.shift(); //remove close icon
    //   var selected = temp.join(' ');
    //   $scope.countrySelected = $scope.contries[selected];
    // };

    // $scope.selectCountry = function() {
    //   $scope.countrySelected = [];
    // };
  }
]);
