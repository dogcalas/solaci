'use strict';

// Setting up route
angular.module('users.admin.routes').config(['$stateProvider', '$translateProvider', 'localStorageServiceProvider',
    function($stateProvider, $translateProvider) {
      $stateProvider
        .state('admin.users', {
          url: '/users',
          templateUrl: 'modules/users/client/views/admin/list-users.client.view.html',
          controller: 'UserListController'
        })
        .state('admin.user', {
          url: '/users/:userId',
          templateUrl: 'modules/users/client/views/admin/view-user.client.view.html',
          controller: 'UserController',
          resolve: {
            userResolve: ['$stateParams', 'Admin', function($stateParams, Admin) {
              return Admin.get({
                userId: $stateParams.userId
              });
            }]
          }
        })
        .state('admin.user-edit', {
          url: '/users/:userId/edit',
          templateUrl: 'modules/users/client/views/admin/edit-user.client.view.html',
          controller: 'UserController',
          resolve: {
            userResolve: ['$stateParams', 'Admin', function($stateParams, Admin) {
              return Admin.get({
                userId: $stateParams.userId
              });
            }]
          }
        });
      $translateProvider.useStaticFilesLoader({
        prefix: 'modules/users/client/translations/trans_',
        suffix: '.json'
      });
      $translateProvider.useSanitizeValueStrategy('escaped');
      $translateProvider.preferredLanguage('es');
    }
]);
