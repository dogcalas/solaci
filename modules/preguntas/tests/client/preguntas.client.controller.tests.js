'use strict';

(function () {
  // Preguntas Controller Spec
  describe('Preguntas Controller Tests', function () {
    // Initialize global variables
    var PreguntasController,
      scope,
      $httpBackend,
      $stateParams,
      $location,
      Authentication,
      Preguntas,
      mockPregunta;

    // The $resource service augments the response object with methods for updating and deleting the resource.
    // If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
    // the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
    // When the toEqualData matcher compares two objects, it takes only object properties into
    // account and ignores methods.
    beforeEach(function () {
      jasmine.addMatchers({
        toEqualData: function (util, customEqualityTesters) {
          return {
            compare: function (actual, expected) {
              return {
                pass: angular.equals(actual, expected)
              };
            }
          };
        }
      });
    });

    // Then we can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_, _Authentication_, _Preguntas_) {
      // Set a new global scope
      scope = $rootScope.$new();

      // Point global variables to injected services
      $stateParams = _$stateParams_;
      $httpBackend = _$httpBackend_;
      $location = _$location_;
      Authentication = _Authentication_;
      Preguntas = _Preguntas_;

      // create mock pregunta
      mockPregunta = new Preguntas({
        _id: '525a8422f6d0f87f0e407a33',
        title: 'An Pregunta about MEAN',
        content: 'MEAN rocks!'
      });

      // Mock logged in user
      Authentication.user = {
        roles: ['user']
      };

      // Initialize the Preguntas controller.
      PreguntasController = $controller('PreguntasController', {
        $scope: scope
      });
    }));

    it('$scope.find() should create an array with at least one pregunta object fetched from XHR', inject(function (Preguntas) {
      // Create a sample preguntas array that includes the new pregunta
      var samplePreguntas = [mockPregunta];

      // Set GET response
      $httpBackend.expectGET('api/preguntas').respond(samplePreguntas);

      // Run controller functionality
      scope.find();
      $httpBackend.flush();

      // Test scope value
      expect(scope.preguntas).toEqualData(samplePreguntas);
    }));

    it('$scope.findOne() should create an array with one pregunta object fetched from XHR using a preguntaId URL parameter', inject(function (Preguntas) {
      // Set the URL parameter
      $stateParams.preguntaId = mockPregunta._id;

      // Set GET response
      $httpBackend.expectGET(/api\/preguntas\/([0-9a-fA-F]{24})$/).respond(mockPregunta);

      // Run controller functionality
      scope.findOne();
      $httpBackend.flush();

      // Test scope value
      expect(scope.pregunta).toEqualData(mockPregunta);
    }));

    describe('$scope.create()', function () {
      var samplePreguntaPostData;

      beforeEach(function () {
        // Create a sample pregunta object
        samplePreguntaPostData = new Preguntas({
          title: 'An Pregunta about MEAN',
          content: 'MEAN rocks!'
        });

        // Fixture mock form input values
        scope.title = 'An Pregunta about MEAN';
        scope.content = 'MEAN rocks!';

        spyOn($location, 'path');
      });

      it('should send a POST request with the form input values and then locate to new object URL', inject(function (Preguntas) {
        // Set POST response
        $httpBackend.expectPOST('api/preguntas', samplePreguntaPostData).respond(mockPregunta);

        // Run controller functionality
        scope.create(true);
        $httpBackend.flush();

        // Test form inputs are reset
        expect(scope.title).toEqual('');
        expect(scope.content).toEqual('');

        // Test URL redirection after the pregunta was created
        expect($location.path.calls.mostRecent().args[0]).toBe('preguntas/' + mockPregunta._id);
      }));

      it('should set scope.error if save error', function () {
        var errorMessage = 'this is an error message';
        $httpBackend.expectPOST('api/preguntas', samplePreguntaPostData).respond(400, {
          message: errorMessage
        });

        scope.create(true);
        $httpBackend.flush();

        expect(scope.error).toBe(errorMessage);
      });
    });

    describe('$scope.update()', function () {
      beforeEach(function () {
        // Mock pregunta in scope
        scope.pregunta = mockPregunta;
      });

      it('should update a valid pregunta', inject(function (Preguntas) {
        // Set PUT response
        $httpBackend.expectPUT(/api\/preguntas\/([0-9a-fA-F]{24})$/).respond();

        // Run controller functionality
        scope.update(true);
        $httpBackend.flush();

        // Test URL location to new object
        expect($location.path()).toBe('/preguntas/' + mockPregunta._id);
      }));

      it('should set scope.error to error response message', inject(function (Preguntas) {
        var errorMessage = 'error';
        $httpBackend.expectPUT(/api\/preguntas\/([0-9a-fA-F]{24})$/).respond(400, {
          message: errorMessage
        });

        scope.update(true);
        $httpBackend.flush();

        expect(scope.error).toBe(errorMessage);
      }));
    });

    describe('$scope.remove(pregunta)', function () {
      beforeEach(function () {
        // Create new preguntas array and include the pregunta
        scope.preguntas = [mockPregunta, {}];

        // Set expected DELETE response
        $httpBackend.expectDELETE(/api\/preguntas\/([0-9a-fA-F]{24})$/).respond(204);

        // Run controller functionality
        scope.remove(mockPregunta);
      });

      it('should send a DELETE request with a valid preguntaId and remove the pregunta from the scope', inject(function (Preguntas) {
        expect(scope.preguntas.length).toBe(1);
      }));
    });

    describe('scope.remove()', function () {
      beforeEach(function () {
        spyOn($location, 'path');
        scope.pregunta = mockPregunta;

        $httpBackend.expectDELETE(/api\/preguntas\/([0-9a-fA-F]{24})$/).respond(204);

        scope.remove();
        $httpBackend.flush();
      });

      it('should redirect to preguntas', function () {
        expect($location.path).toHaveBeenCalledWith('preguntas');
      });
    });
  });
}());
