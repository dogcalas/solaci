'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Pregunta = mongoose.model('Pregunta'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app, agent, credentials, user, pregunta;

/**
 * Pregunta routes tests
 */
describe('Pregunta CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new pregunta
    user.save(function () {
      pregunta = {
        title: 'Pregunta Title',
        content: 'Pregunta Content'
      };

      done();
    });
  });

  it('should be able to save an pregunta if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new pregunta
        agent.post('/api/preguntas')
          .send(pregunta)
          .expect(200)
          .end(function (preguntaSaveErr, preguntaSaveRes) {
            // Handle pregunta save error
            if (preguntaSaveErr) {
              return done(preguntaSaveErr);
            }

            // Get a list of preguntas
            agent.get('/api/preguntas')
              .end(function (preguntasGetErr, preguntasGetRes) {
                // Handle pregunta save error
                if (preguntasGetErr) {
                  return done(preguntasGetErr);
                }

                // Get preguntas list
                var preguntas = preguntasGetRes.body;

                // Set assertions
                (preguntas[0].user._id).should.equal(userId);
                (preguntas[0].title).should.match('Pregunta Title');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an pregunta if not logged in', function (done) {
    agent.post('/api/preguntas')
      .send(pregunta)
      .expect(403)
      .end(function (preguntaSaveErr, preguntaSaveRes) {
        // Call the assertion callback
        done(preguntaSaveErr);
      });
  });

  it('should not be able to save an pregunta if no title is provided', function (done) {
    // Invalidate title field
    pregunta.title = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new pregunta
        agent.post('/api/preguntas')
          .send(pregunta)
          .expect(400)
          .end(function (preguntaSaveErr, preguntaSaveRes) {
            // Set message assertion
            (preguntaSaveRes.body.message).should.match('Title cannot be blank');

            // Handle pregunta save error
            done(preguntaSaveErr);
          });
      });
  });

  it('should be able to update an pregunta if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new pregunta
        agent.post('/api/preguntas')
          .send(pregunta)
          .expect(200)
          .end(function (preguntaSaveErr, preguntaSaveRes) {
            // Handle pregunta save error
            if (preguntaSaveErr) {
              return done(preguntaSaveErr);
            }

            // Update pregunta title
            pregunta.title = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing pregunta
            agent.put('/api/preguntas/' + preguntaSaveRes.body._id)
              .send(pregunta)
              .expect(200)
              .end(function (preguntaUpdateErr, preguntaUpdateRes) {
                // Handle pregunta update error
                if (preguntaUpdateErr) {
                  return done(preguntaUpdateErr);
                }

                // Set assertions
                (preguntaUpdateRes.body._id).should.equal(preguntaSaveRes.body._id);
                (preguntaUpdateRes.body.title).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of preguntas if not signed in', function (done) {
    // Create new pregunta model instance
    var preguntaObj = new Pregunta(pregunta);

    // Save the pregunta
    preguntaObj.save(function () {
      // Request preguntas
      request(app).get('/api/preguntas')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single pregunta if not signed in', function (done) {
    // Create new pregunta model instance
    var preguntaObj = new Pregunta(pregunta);

    // Save the pregunta
    preguntaObj.save(function () {
      request(app).get('/api/preguntas/' + preguntaObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('title', pregunta.title);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single pregunta with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/preguntas/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Pregunta is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single pregunta which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent pregunta
    request(app).get('/api/preguntas/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No pregunta with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an pregunta if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new pregunta
        agent.post('/api/preguntas')
          .send(pregunta)
          .expect(200)
          .end(function (preguntaSaveErr, preguntaSaveRes) {
            // Handle pregunta save error
            if (preguntaSaveErr) {
              return done(preguntaSaveErr);
            }

            // Delete an existing pregunta
            agent.delete('/api/preguntas/' + preguntaSaveRes.body._id)
              .send(pregunta)
              .expect(200)
              .end(function (preguntaDeleteErr, preguntaDeleteRes) {
                // Handle pregunta error error
                if (preguntaDeleteErr) {
                  return done(preguntaDeleteErr);
                }

                // Set assertions
                (preguntaDeleteRes.body._id).should.equal(preguntaSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an pregunta if not signed in', function (done) {
    // Set pregunta user
    pregunta.user = user;

    // Create new pregunta model instance
    var preguntaObj = new Pregunta(pregunta);

    // Save the pregunta
    preguntaObj.save(function () {
      // Try deleting pregunta
      request(app).delete('/api/preguntas/' + preguntaObj._id)
        .expect(403)
        .end(function (preguntaDeleteErr, preguntaDeleteRes) {
          // Set message assertion
          (preguntaDeleteRes.body.message).should.match('User is not authorized');

          // Handle pregunta error error
          done(preguntaDeleteErr);
        });

    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Pregunta.remove().exec(done);
    });
  });
});
