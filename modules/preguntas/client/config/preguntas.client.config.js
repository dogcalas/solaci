'use strict';

// Configuring the Preguntas module
angular.module('preguntas').run(['Menus',
  function (Menus) {
    // Add the preguntas dropdown item
    Menus.addMenuItem('topbar', {
      title: 'Preguntas',
      state: 'preguntas.list',
      roles: ['admin']
    });

    // // Add the dropdown list item
    // Menus.addSubMenuItem('topbar', 'preguntas', {
    //   title: 'List Preguntas',
    //   state: 'preguntas.list',
    //   roles: ['admin']
    // });

    // // Add the dropdown create item
    // Menus.addSubMenuItem('topbar', 'preguntas', {
    //   title: 'Create Preguntas',
    //   state: 'preguntas.create',
    //   roles: ['admin']
    // });
  }
]);
