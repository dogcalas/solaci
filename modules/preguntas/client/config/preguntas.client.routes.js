'use strict';

// Setting up route
angular.module('preguntas').config(['$stateProvider',
  function ($stateProvider) {
    // Preguntas state routing
    $stateProvider
      .state('preguntas', {
        abstract: true,
        url: '/preguntas',
        template: '<ui-view/>'
      })
      .state('preguntas.list', {
        url: '',
        templateUrl: 'modules/preguntas/client/views/list-preguntas.client.view.html'
      })
      .state('preguntas.create', {
        url: '/create',
        templateUrl: 'modules/preguntas/client/views/create-pregunta.client.view.html',
        data: {
          roles: ['admin']
        }
      })
      .state('preguntas.view', {
        url: '/:preguntaId',
        templateUrl: 'modules/preguntas/client/views/view-pregunta.client.view.html'
      })
      .state('preguntas.edit', {
        url: '/:preguntaId/edit',
        templateUrl: 'modules/preguntas/client/views/edit-pregunta.client.view.html',
        data: {
          roles: ['admin']
        }
      });
  }
]);
