'use strict';

//Preguntas service used for communicating with the preguntas REST endpoints
angular.module('preguntas').factory('Preguntas', ['$resource',
  function ($resource) {
    return $resource('api/preguntas/:preguntaId', {
      preguntaId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);


angular.module('preguntas').factory('Datestart', ['$resource',
  function ($resource) {
    return $resource('api/date', {});
  }
]);

