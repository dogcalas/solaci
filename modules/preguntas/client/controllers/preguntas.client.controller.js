'use strict';

// Preguntas controller
angular.module('preguntas').controller('PreguntasController', ['$scope', '$stateParams', '$timeout', '$window', '$location', 'Authentication', 'Preguntas', 'Datestart', 'FileUploader', '$http',
  function ($scope, $stateParams, $timeout, $window, $location, Authentication, Preguntas, Datestart, FileUploader, $http) {
    $scope.authentication = Authentication;
    $scope.options = [];
    $scope.time = 30;
    
    //Modifica la fecha de inicio del proyecto
    $scope.startdate = Datestart.get();
    $scope.restartServer = function () {
      if (confirm('¿Esta seguro que desea reiniciar el servidor?')) {
        $http.get('api/restart').success(function(data, status, headers, config) {
          alert("Servidor reiniciado");
        }).error(function(data, status, headers, config) {
          // Handle the error
          console.log("Ocurrio un error al reiniciar el servidor")
        });
      }

    }
    $scope.changeDate = function () {
      if (confirm('¿Esta seguro que desea cambiar la fecha de incio?')) {
        var dates = new Datestart({
          date: $scope.startdate.date,
        });
        dates.$save(function (response) {});
      }


    }

    // Adiciona una opcion al arreglo de opciones de la pregunta
    $scope.addOption = function() {
      // alert($scope.option);
      if ($scope.pregunta) {
        $scope.pregunta.options.push({
          name: $scope.option,
          correct: false
        });
      } else {
        $scope.options.push({
          name: $scope.option,
          correct: false
        });
      }
      
      $scope.option = '';
    };

    // Elimina una opcion al arreglo de opciones de la pregunta
    $scope.delOption = function(option) {
      if (confirm('¿Desea eliminar la opción ' + option.name + '?')) {
        if ($scope.pregunta) {
          // Busco la posicion de la opcion en el arreglo y la elimino, en key va la posicion y value es el objeto
          angular.forEach($scope.pregunta.options, function (value, key) {
            // Si sus $$hashKey son iguales, cojo su posicion y la elimino
            if (value.$$hashKey === option.$$hashKey) {
              $scope.pregunta.options.splice(key, 1);
            }
          });
        } else {
          angular.forEach($scope.options, function (value, key) {
            // Si sus $$hashKey son iguales, cojo su posicion y la elimino
            if (value.$$hashKey === option.$$hashKey) {
              $scope.options.splice(key, 1);
            }
          });
        }
      }
    };

    // Marca la opcion como correcta y las demas las pone false
    $scope.marcarCorrecta = function(option) {
      // console.log(option);
      if ($scope.pregunta) {
        for (var j = $scope.pregunta.options.length - 1; j >= 0; j--) {
          // console.log($scope.options[i].$$hashKey === option.$$hashKey);
          if ($scope.pregunta.options[j].$$hashKey === option.$$hashKey) {
            $scope.pregunta.options[j].correct = true;
          } else {
            $scope.pregunta.options[j].correct = false;  
          }
        }
      } else {
        for (var i = $scope.options.length - 1; i >= 0; i--) {
          // console.log($scope.options[i].$$hashKey === option.$$hashKey);
          if ($scope.options[i].$$hashKey === option.$$hashKey) {
            $scope.options[i].correct = true;
          } else {
            $scope.options[i].correct = false;  
          }
        }
      }
    };

    // Create new Pregunta
    $scope.create = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'preguntaForm');

        return false;
      }

      // Create new Pregunta object
      var pregunta = new Preguntas({
        title: this.title,
        description: this.description,
        time: this.time,
        order: this.order,
        imageURL: $scope.imageURL,
        options: $scope.options
      });

      // Redirect after save
      pregunta.$save(function (response) {
        $location.path('preguntas');

        // Clear form fields
        $scope.title = '';
        $scope.description = '';
        $scope.time = 30;
        $scope.order = '';
        $scope.options = [];
        $scope.imageURL = 'modules/users/client/img/profile/default.png';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Remove existing Pregunta
    $scope.remove = function (pregunta) {
      if (confirm('¿Desea eliminar la pregunta: ' + pregunta.title + '?')) {
        if (pregunta) {
          pregunta.$remove();

          for (var i in $scope.preguntas) {
            if ($scope.preguntas[i] === pregunta) {
              $scope.preguntas.splice(i, 1);
            }
          }
        } else {
          $scope.pregunta.$remove(function () {
            $location.path('preguntas');
          });
        }
      }
    };

    // Update existing Pregunta
    $scope.update = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'preguntaForm');

        return false;
      }

      var pregunta = $scope.pregunta;

      pregunta.$update(function () {
        $location.path('preguntas/' + pregunta._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Find a list of Preguntas
    $scope.find = function () {
      $scope.preguntas = Preguntas.query();
    };

    // Find existing Pregunta
    $scope.findOne = function () {
      $scope.pregunta = Preguntas.get({
        preguntaId: $stateParams.preguntaId
      });
      $scope.imageURL = $scope.pregunta.imageURL;
    };

    $scope.printOpciones = function(options) {
      var opts = '';
      // console.log(options.length);
      angular.forEach(options, function (value, key) {
        // Si sus $$hashKey son iguales, cojo su posicion y la elimino
        opts += value.name;
        if (key < options.length - 1) {
          opts += ', ';
        }
        // console.log(key);
        // if (key) {}
        // if (value.$$hashKey === option.$$hashKey) {
        //   $scope.options.splice(key, 1);
        // }
      });
      return opts;
    };

    //Para subir imagenes
    // $scope.imageURL = $scope.user.profileImageURL;
    $scope.imageURL = 'modules/users/client/img/profile/default.png';

    // Create file uploader instance
    $scope.uploader = new FileUploader({
      url: 'api/pregunta/picture',
      alias: 'newPicture'
    });

    // Set file uploader image filter
    $scope.uploader.filters.push({
      name: 'imageFilter',
      fn: function (item, options) {
        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
      }
    });

    // Called after the user selected a new picture file
    $scope.uploader.onAfterAddingFile = function (fileItem) {
      if ($window.FileReader) {
        var fileReader = new FileReader();
        fileReader.readAsDataURL(fileItem._file);

        fileReader.onload = function (fileReaderEvent) {
          $timeout(function () {
            $scope.imageURL = fileReaderEvent.target.result;
            $scope.uploadPicture();
          }, 0);
        };
      }
    };

    // Called after the user has successfully uploaded a new picture
    $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
      // Show success message
      $scope.success = true;

      // Populate user object
      if ($scope.pregunta) {
        $scope.pregunta.imageURL = response.imageURL;
      }
      $scope.imageURL = response.imageURL;

      // Clear upload buttons
      $scope.cancelUpload();
    };

    // Called after the user has failed to uploaded a new picture
    $scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
      // Clear upload buttons
      // $scope.cancelUpload();
      console.log(headers);
      console.log(response);

      // Show error message
      $scope.error = response.message;
    };

    // Change user profile picture
    $scope.uploadPicture = function () {
      // Clear messages
      $scope.success = $scope.error = null;

      // Start upload
      $scope.uploader.uploadAll();
    };

    // Cancel the upload process
    $scope.cancelUpload = function () {
      $scope.uploader.clearQueue();
      $scope.imageURL = $scope.pregunta.imageURL;
    };
  }
]);
