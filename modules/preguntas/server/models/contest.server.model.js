'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

var fields = {
  hourini: {
    type: Date,
    default: Date.now
  },
  initialimg: {
    type: String,
    default: 'modules/users/client/img/profile/default.png'
  },
  description: {
    type: String
  },
  created: {
    type: Date,
    default: Date.now
  }
};

var contestSchema = new Schema(fields);
mongoose.model('Contest', contestSchema);
