'use strict';

var async = require('async');
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/*
 Options Schema
 */
var OptionSchema = new Schema({
  name: String,
  correct: Boolean
});

/**
 * Answers Schema
 */
var AnswerSchema = new Schema({
  option: {
    type: Schema.ObjectId,
    ref: 'Opcion'
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  time: {
    type: Number
  },
  fake: {
    type:Boolean,
    default:false
  }
});

/**
 * Pregunta Schema
 */
var PreguntaSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  title: {
    type: String,
    default: '',
    trim: true,
    required: 'Title cannot be blank'
  },
  description: {
    type: String,
    default: '',
    trim: true
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  time: {
    type: Number,
    required: 'The time is required',
    min: 5
  },
  imageURL: {
    type: String,
    default: 'modules/users/client/img/profile/default.png'
  },
  order: {
    type: Number,
    default: 0,
    unique: true
  },
  options: [OptionSchema],
  answers: [AnswerSchema]
});

/**
 * Obtiene las estadisticas de la pregunta
 * @param {ObjectId} idPregunta - Id de la pregunta
 * @return callbak
 * */
PreguntaSchema.statics.getEstadisticas = function (idPregunta, callback) {
  return mongoose.model('Pregunta').getCantUsuariosActivados(function (err, cant_usuarios_activados) {
    if (err) return callback(err);
    mongoose.model('Pregunta').getCantPreguntas(function (err, preguntas) {
      if (err) return callback(err);
      mongoose.model('Pregunta').getCantRespuestasByPregunta(idPregunta, true, function (err, respuestas_correctas) {
        if (err) return callback(err);
        mongoose.model('Pregunta').getCantRespuestasByPregunta(idPregunta, false, function (err, respuestas_incorrectas) {
          if (err) return callback(err);
            mongoose.model('Pregunta').getTiempoPromedioTodasRespuestasByPregunta(idPregunta, function (err, tiempo_promedio) {
              if (err) return callback(err);
              var stats = {
                'cant_usuarios_activados': cant_usuarios_activados,
                'preguntas': preguntas,
                'respuestas_correctas': respuestas_correctas,
                'respuestas_incorrectas': respuestas_incorrectas,
                'tiempo_promedio': Math.round(tiempo_promedio)
              };
              return callback(null, stats);
            });
          });
        });
      });
    });

};

/**
 * Obtiene la cantidad de usuarios activados que no han respondido la pregunta
 * @param  {ObjectId} idPregunta - Id de la pregunta
 * @return callbak
 * */
PreguntaSchema.statics.getCantActivadosSinRespuestasByPregunta = function (idPregunta, callback) {
  return mongoose.model('Pregunta').getCantUsuariosActivados(function (err, cantActivados) {
    if (err) return callback(err, cantActivados);
    mongoose.model('Pregunta').getRespuestasByPregunta(idPregunta, function (err, respuestas) {
      if (err) return callback(err, respuestas);
      var cantRespuestas = cantActivados - respuestas.length;
      return callback(null, (cantRespuestas < 0) ? 0 : cantRespuestas);
    });
  });
};

/**
 * Obtiene la cantidad de preguntas sin responder
 * @param  {ObjectId} idPregunta - Id de la pregunta
 * @return callbak
 * */
PreguntaSchema.statics.getCantPreguntasSinRespuestasByUsuario = function (idUsuario, callback) {
  return mongoose.model('Pregunta').count({}, function (err, cantPreguntas) {
    if (err) return callback(err, cantPreguntas);
    mongoose.model('Pregunta').count({
      'answers.user': idUsuario
    }, function (err, respuestas) {
      if (err) return callback(err, respuestas);
      var cantRespuestas = cantPreguntas - respuestas;
      return callback(null, (cantRespuestas < 0) ? 0 : cantRespuestas);
    });
  });
};

/**
 * Obtiene la cantidad de usuarios activados que no han respondido preguntas
 * @param  {ObjectId} idPregunta - Id de la pregunta
 * @return callbak
 * */
PreguntaSchema.statics.getCantActivadosSinRespuestas = function (callback) {
  return mongoose.model('Pregunta').getCantUsuariosActivados(function (err, cantActivados) {
    if (err) return callback(err, cantActivados);
    mongoose.model('Pregunta').getCantTotalRespuestas(function (err, respuestas) {
      if (err) return callback(err, respuestas);
      mongoose.model('Pregunta').getCantPreguntas(function (err, preguntas) {
        if (err) return callback(err, respuestas);
        var cantRespuestas = (cantActivados * preguntas) - respuestas;
        return callback(null, (cantRespuestas < 0) ? 0 : cantRespuestas);
      });
    });
  });
};

/**
 * Obtiene la cantidad de usuarios activados
 * @return callbak
 * */
PreguntaSchema.statics.getCantUsuariosActivados = function (callback) {
  return mongoose.model('User').count({
    $and: [{
      roles: 'concursante'
    }, {
      roles: {
        $nin: ['admin', 'moderador']
      }
    }]
  }, callback);
};


/**
 * Obtiene la cantidad de usuarios registrados que no han sido activados
 * @return callbak
 * */
PreguntaSchema.statics.getCantUsuariosSinActivar = function (callback) {
  return mongoose.model('User').count({
    $and: [{
      roles: 'user'
    }, {
      roles: {
        $nin: ['admin', 'moderador', 'concursante']
      }
    }]
  }, callback);
};

/**
 * Obtiene la cantidad respuestas correctas/incorrectas dadas por los usuarios
 * @param {Boolean} correct - Define el si son las correctas o las incorrectas. True para las correctas, False para las incorrectas
 * @return callbak
 * */
PreguntaSchema.statics.getCantRespuestas = function (correct, callback) {
  this.model('Pregunta').find({}, 'answers options')
    .exec(function (err, result) {
      if (err) {
        return callback(err, result);
      }
      var counter = 0;
      if (result)
        result.forEach(function (question) {
          if (question)
            question.answers.forEach(function (ans) {
              if (question.options.id(ans.option).correct === correct) {
                counter++;
              }
            });

        });
      callback(null, counter);
    });
};

/**
 * Obtiene la cantidad total de respuestas dadas por los usuarios
 * @param {Boolean} correct - Define el si son las correctas o las incorrectas. True para las correctas, False para las incorrectas
 * @return callbak
 * */
PreguntaSchema.statics.getCantTotalRespuestas = function (callback) {
  this.model('Pregunta').find({}, 'answers options')
    .exec(function (err, result) {
      if (err) {
        return callback(err, result);
      }
      var counter = 0;
      if (result)
        result.forEach(function (question) {
          if (question)
            question.answers.forEach(function (ans) {
              counter++;
            });

        });
      callback(null, counter);
    });
};

/**
 * Obtiene el tiempo promedio respuestas correctas/incorrectas dadas por los usuarios
 * @param {Boolean} correct - Define el si son las correctas o las incorrectas. True para las correctas, False para las incorrectas
 * @return callbak
 * */
PreguntaSchema.statics.getTiempoPromedioRespuestas = function (correct, callback) {
  this.model('Pregunta').find({}, 'answers options')
    .exec(function (err, result) {
      if (err) {
        return callback(err, result);
      }
      var counter = 0;
      var sum = 0;
      if (result)
        result.forEach(function (question) {
          if (question)
            question.answers.forEach(function (ans) {
              if (question.options.id(ans.option).correct === correct) {
                counter++;
                sum += ans.time;
              }
            });

        });
      callback(null, sum / counter);
    });
};

/**
 * Obtiene el tiempo promedio de todas las respuestas dadas por los usuarios
 * @param {Boolean} correct - Define el si son las correctas o las incorrectas. True para las correctas, False para las incorrectas
 * @return callbak
 * */
PreguntaSchema.statics.getTiempoPromedioTodasRespuestas = function (callback) {
  this.model('Pregunta').find({}, 'answers options')
    .exec(function (err, result) {
      if (err) {
        return callback(err, result);
      }
      var counter = 0;
      var sum = 0;
      if (result)
        result.forEach(function (question) {
          if (question)
            question.answers.forEach(function (ans) {
              counter++;
              sum += ans.time;
            });

        });
      callback(null, sum / counter);
    });
};

/**
 * Obtiene el tiempo promedio de respuestas correctas/incorrectas en una pregunta
 * @param {ObjectId} idPregunta - Id de la pregunta
 * @param {Boolean} correct - Define el si son las correctas o las incorrectas. True para las correctas, False para las incorrectas
 * @return callbak
 * */
PreguntaSchema.statics.getTiempoPromedioRespuestasByPregunta = function (idPregunta, correct, callback) {
  this.model('Pregunta').findOne({'_id': idPregunta}, 'answers options')
    .exec(function (err, result) {
      if (err) {
        return callback(err, result);
      }
      var counter = 0;
      var sum = 0;
      if (result) {
        result.answers.forEach(function (ans) {
          if (result.options.id(ans.option).correct === correct) {
            counter++;
            sum += ans.time;
          }
        });
      }

      callback(null, sum / counter);
    });
};
/**
 * Obtiene el tiempo promedio de todas las respuestas de una pregunta
 * @param {ObjectId} idPregunta - Id de la pregunta
 * @param {Boolean} correct - Define el si son las correctas o las incorrectas. True para las correctas, False para las incorrectas
 * @return callbak
 * */
PreguntaSchema.statics.getTiempoPromedioTodasRespuestasByPregunta = function (idPregunta, callback) {
  this.model('Pregunta').findOne({
      '_id': idPregunta
    }, 'answers options')
    .exec(function (err, result) {
      if (err) {
        return callback(err, result);
      }
      var counter = 0;
      var sum = 0;
      if (result) {
        result.answers.forEach(function (ans) {
          counter++;
          sum += ans.time;
        });
      }

      callback(null, sum / counter);
    });
};


/**
 * Obtiene el tiempo promedio de todas las respuestas de una pregunta
 * @param {ObjectId} idPregunta - Id de la pregunta
 * @param {Boolean} correct - Define el si son las correctas o las incorrectas. True para las correctas, False para las incorrectas
 * @return callbak
 * */
PreguntaSchema.statics.getTiempoPromedioTodasRespuestasByUsuario = function (idUsuario, callback) {
  this.model('Pregunta').find({
    'answers.user': idUsuario
  }, 'answers options').exec(function (err, result) {
    if (err) {
      return callback(err, result);
    }
    var counter = 0;
    var sum = 0;
    if (result) {
      result.forEach(function (question) {
        question.answers.forEach(function (ans) {
          if (ans.user.equals(idUsuario)) {
            counter++;
            sum += ans.time;
          }
        });
      });
    }

    callback(null, sum / counter);
  });
};

/**
 * Obtiene la cantidad respuestas correctas/incorrectas dadas por los usuarios en una pregunta
 * @param {ObjectId} idPregunta - Id de la pregunta
 * @param {Boolean} correct - Define el si son las correctas o las incorrectas. True para las correctas, False para las incorrectas
 * @return callbak
 * */
PreguntaSchema.statics.getCantRespuestasByPregunta = function (idPregunta, correct, callback) {
  this.model('Pregunta').findOne({
    '_id': idPregunta
  }, 'answers options').exec(function (err, result) {
    if (err) {
      return callback(err, result);
    }
    var counter = 0;
    if (result) {
      result.answers.forEach(function (ans) {
        if (result.options.id(ans.option).correct === correct) {
          counter++;
        }
      });
    }

    callback(null, counter);
  });
};

/**
 * Obtiene la cantidad respuestas dadas por un usuario
 * @param {ObjectId} idUsuario - Id del usuario
 * @param {Boolean} correct - Define el si son las correctas o las incorrectas. True para las correctas, False para las incorrectas
 * @return callbak
 * */
PreguntaSchema.statics.getCantRespuestasTotalesByUsuario = function (idUsuario, callback) {
  this.model('Pregunta').find({
    'answers.user': idUsuario
  }, 'answers options').exec(function (err, result) {
    if (err) {
      return callback(err, result);
    }
    var counter = 0;
    if (result) {
      result.forEach(function (question) {
        question.answers.forEach(function (ans) {
          if (ans.user.equals(idUsuario)) {
            counter++;
          }
        });
      });
    }

    callback(null, counter);
  });
};


/**
 * Obtiene los datos de una pregunta
 * @param {ObjectId} idPregunta - Id de la pregunta
 * @return callbak
 * */
PreguntaSchema.statics.getPregunta = function (idPregunta, callback) {
  return this.model('Pregunta').find({
    '_id': idPregunta
  }, callback);
};

/**
 * Obtiene los datos de una pregunta, mas información de la siguiente pregunta
 * @param {ObjectId} idPregunta - Id de la pregunta
 * @return callbak
 * */
PreguntaSchema.statics.getPreguntaModerador = function (idPregunta, callback) {
  return this.model('Pregunta').find()
    .sort({
      order: 1
    }).exec(function (err, questions) {
      var exit = {};
      if (questions)
        questions.forEach(function (question, indice) {
          if (question._id.equals(idPregunta)) {
            exit.first = question;
            if (indice !== questions.length - 1) {
              exit.second = questions[indice + 1];
            }
            return callback(null, exit);
          }
        });
    });
};

/**
 * Obtiene las preguntas ordenadas
 * @return callbak
 * */
PreguntaSchema.statics.getPreguntasByOrden = function (callback) {
  return this.model('Pregunta')
    .find()
    .sort({
      order: 1
    })
    .exec(callback);
};

/**
 * Obtiene la cantidad de preguntas
 * @return callbak
 * */
PreguntaSchema.statics.getCantPreguntas = function (callback) {
  return this.model('Pregunta')
    .count()
    .exec(callback);
};
/**
 * Obtiene la respuesta correcta de una pregunta
 * @return callbak
 * */
PreguntaSchema.statics.getRespuestaCorrecta = function (idPregunta, callback) {
  this.model('Pregunta').findOne({
    '_id': idPregunta
  }, 'options', function (err, opts) {
    //console.log(options);
    if (err) {
      return callback(err, opts);
    }
    if (opts)
      opts.options.forEach(function (val) {
        if (val.correct) {
          return callback(null, val);
        }

      });
  });
};

/**
 * Obtiene las estadisticas finales
 * @return callbak
 * */
PreguntaSchema.statics.getEstadisticasFinales = function (callback) {
  return mongoose.model('Pregunta').getCantRespuestas(true, function (err, respuestas_correctas) {
    if (err) return callback(err);
    mongoose.model('Pregunta').getCantRespuestas(false, function (err, respuestas_incorrectas) {
      if (err) return callback(err);
      mongoose.model('Pregunta').getCantActivadosSinRespuestas(function (err, abstenciones) {
        if (err) return callback(err);
        mongoose.model('Pregunta').getTiempoPromedioTodasRespuestas(function (err, tiempo_promedio) {
          if (err) return callback(err);
          var stats = {
            'respuestas_correctas': respuestas_correctas,
            'respuestas_incorrectas': respuestas_incorrectas,
            'abstenciones': abstenciones,
            'tiempo_promedio': Math.round(tiempo_promedio)
          };
          return callback(null, stats);
        });
      });
    });
  });
};

/**
 * Obtiene las estadisticas de los usuarios
 * @return callbak
 * */
PreguntaSchema.statics.getEstadisticasUsuarios = function (callback) {
  this.model('Pregunta').find({}, 'answers options')
    .exec(function (err, result) {
      if (err) {
        return callback(err, result);
      }
      var mapUserStats = new Map();
      if (result) {
        result.forEach(function (question) {
          question.answers.forEach(function (ans) {
            if (!mapUserStats.has(ans.user.toString())) {
              mapUserStats.set(ans.user.toString(), {
                correct: 0,
                incorrect: 0,
                time: 0,
                abstenciones: result.length
              });
            }
            if (question.options.id(ans.option).correct === true) {
              mapUserStats.get(ans.user.toString()).correct++;
              mapUserStats.get(ans.user.toString()).time += ans.time;
            } else {
              mapUserStats.get(ans.user.toString()).incorrect++;
            }
            mapUserStats.get(ans.user.toString()).abstenciones--;

          });
        });
      }
      callback(null, mapUserStats);
    });
};

/**
 * Obtiene las estadisticas de las preguntas
 * @return callbak
 * */
PreguntaSchema.statics.getEstadisticasPreguntas = function (callback) {
  this.model('Pregunta')
    .find({}, 'answers options')
    .sort({
      order: 1
    })
    .exec(function (err, result) {
      if (err) {
        return callback(err, result);
      }
      var rquestions = [];
      var idx = 0;
      if (result) {
        result.forEach(function (question) {
          rquestions[idx] = {
            'correctas': 0,
            'incorrectas': 0,
            'abstenciones': 0,
            'tiempo_total': 0,
            'tiempo_promedio': 0
          };
          question.answers.forEach(function (ans) {
            if (question.options.id(ans.option).correct === true) {
              rquestions[idx].correctas++;
            } else {
              rquestions[idx].incorrectas++;
            }
            rquestions[idx].tiempo_total += ans.time;
            rquestions[idx].abstenciones++;
          });
          rquestions[idx].tiempo_promedio = (rquestions[idx].abstenciones)?(rquestions[idx].tiempo_total/rquestions[idx].abstenciones):0;
          idx++;
        });
      }
      callback(null, rquestions);
    });
};


/**
 * Elimina las respuestas de la base de datos
 * @return callbak
 * */
PreguntaSchema.statics.removeAnswers = function (callback) {
  this.model('Pregunta').update({}, {answers: []}, {multi: true}, function (err, raw) {
    if (err) return callback(err);
    callback(null, raw);
  });
};


/**
 * Obtiene el usuario ganador
 * @return callbak
 * */
PreguntaSchema.statics.getWinner = function (callback) {
  this.model('Pregunta').find({}, 'answers options')
    .populate({
      path: 'answers.user',
      model: 'User'
    })
    .exec(function (err, result) {
      if (err) {
        return callback(err, result);
      }
      var mapUsers = new Map();

      if (result)
        result.forEach(function (question) {
          if (question)
            question.answers.forEach(function (ans) {
              if (question.options.id(ans.option).correct === true && ans.fake) {
                if (mapUsers.has(ans.user._id)) {
                  mapUsers.get(ans.user._id).correct++;
                  mapUsers.get(ans.user._id).time += ans.time;
                } else {
                  mapUsers.set(ans.user._id, {
                    correct: 1,
                    time: ans.time
                  });
                }
              }
            });
        });
      var counter = 0;
      var sum = 1459102010083;
      var winner = 'NADIE';
      mapUsers.forEach(function (value, key) {
        if (value.correct > counter) {
          winner = key;
          counter = value.correct;
          sum = value.time;
        } else if (value.correct === counter) {
          if (value.time < sum) {
            winner = key;
            counter = value.correct;
            sum = value.time;
          }
        }

      })
      if(winner !== 'NADIE'){
        mongoose.model('User').findOne({
          _id: winner
        }, function (err, user) {
          if (err) return callback(err, user);
          user = user.toObject();
          user.respuestas_correctas = counter;
          user.tiempo_respuestas = sum / 1000;
          return callback(null, user);
        });
      } else {
        return callback(null, {
          "displayName" : "Sin ganador",
          "lastName" : "Sin",
          "firstName" : "ganador",
        });
         
      }

    });
};

/**
 * Obtiene las opciones de una pregunta
 * @param {ObjectId} idPregunta - Id de la pregunta
 * @return callbak
 * */
PreguntaSchema.statics.getOpcionesByPregunta = function (idPregunta, callback) {
  this.model('Pregunta').findOne({
    '_id': idPregunta
  }, {
    'options.name': 1,
    '_id': false
  }, function (err, opts) {
    if (err) {
      return callback(err, opts);
    }
    return callback(err, opts);
  });
};

/**
 * Obtiene la respuesta dada por el usuario a una pregunta
 * @param {ObjectId} idPregunta - Id de la pregunta
 * @return callbak
 * */
PreguntaSchema.statics.getOpcionByPreguntaUsuario = function (idPregunta, idUsuario, callback) {
  this.model('Pregunta').findOne({
      '_id': idPregunta,
      'answers.user': idUsuario
    }, 'answers options')
    ////Esto es por si necesita obtener los datos del usuario. Por Ahora lo comento.
    //    .populate( {
    //        path: 'answers.user',
    //        model: 'User'
    //    })
    .exec(function (err, result) {
      if (err) {
        return callback(err, result);
      }
      callback(null, result.options.id(result.answers[0].option));
    });
};

/**
 * Obtiene las respuestas dadas a una pregunta
 * @param {ObjectId} idPregunta - Id de la pregunta
 * @return callbak
 * */
PreguntaSchema.statics.getRespuestasByPregunta = function (idPregunta, callback) {
  this.model('Pregunta').findOne({
      '_id': idPregunta
    }, 'answers options')
    .exec(function (err, result) {
      if (err) {
        return callback(err, result);
      }
      callback(null, result.answers);
    });
};


/**
 * Permite insertar una respuesta a una pregunta
 * @param {ObjectId} idPregunta - Id de la pregunta a insertar
 * @param {Answer} respuesta - Un objeto con el siguiente formato {option:ObjectId, user:ObjectId, time:Number}
 * @return callbak
 * */
PreguntaSchema.statics.insertRespuesta = function (idPregunta, respuesta, callback) {
  var Answer = mongoose.model('Respuesta', AnswerSchema);
  var ans = new Answer(respuesta);
  this.findOneAndUpdate({
    _id: idPregunta,
    'answers.user': {
      $ne: respuesta.user
    }
  }, {
    $addToSet: {
      answers: ans
    }
  }, function (error, question) {
    if (error) {
      return callback(error, respuesta);
    }
    return callback(null, respuesta);
  });
};

mongoose.model('Opcion', OptionSchema);
mongoose.model('Pregunta', PreguntaSchema);
