'use strict';

/**
 * Module dependencies.
 */
var contestPolicy = require('../policies/contest.server.policy'),
  contest = require('../controllers/contest.server.controller');

module.exports = function (app) {
  // contest collection routes
  app.route('/api/contest').all(contestPolicy.isAllowed)
    .get(contest.list)

  // Single fecha routes
  app.route('/api/date')
      .get(contest.readDate)
      .post(contest.updateDate)

  // Single fecha routes
  app.route('/api/restart')
      .get(contest.restart)
};

