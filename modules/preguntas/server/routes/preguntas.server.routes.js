'use strict';

/**
 * Module dependencies.
 */
var preguntasPolicy = require('../policies/preguntas.server.policy'),
  preguntas = require('../controllers/preguntas.server.controller');

module.exports = function (app) {
  // Preguntas collection routes
  app.route('/api/preguntas').all(preguntasPolicy.isAllowed)
    .get(preguntas.list)
    .post(preguntas.create);

  // Single pregunta routes
  app.route('/api/preguntas/:preguntaId').all(preguntasPolicy.isAllowed)
    .get(preguntas.read)
    .put(preguntas.update)
    .delete(preguntas.delete);

  // Administracion de imagenes
  app.route('/api/pregunta/picture').post(preguntas.changePicture);

  // Finish by binding the pregunta middleware
  app.param('preguntaId', preguntas.preguntaByID);
};
