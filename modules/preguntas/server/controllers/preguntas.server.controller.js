'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Pregunta = mongoose.model('Pregunta'),
  multer = require('multer'),
  config = require(path.resolve('./config/config')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  server_state = require(path.resolve('./config/lib/server_state'))();


Pregunta.insertRespuesta('56e9cc1ac5a5d0fa52d1f4cb', {
  option: '56e9cc1ac5a5d0fa52d1f4cd',
  user: '56e7280f3602bea869613b82',
  time: 2000
}, function(err, data) {
  if (err) {
    console.log(err);
  }
  // console.log(data);
});

/**
 * Create a pregunta
 */
exports.create = function (req, res) {
  // console.log(req.body);
  console.log('Creando pregunta');
  var pregunta = new Pregunta(req.body);
  pregunta.user = req.user;

  pregunta.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      Pregunta.getPreguntasByOrden(function (err, result) {
        if (!err) {
          server_state.setListaPreguntas(result);
          res.json(pregunta);
        }else {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        }

      });
    }
  });
};

/**
 * Show the current pregunta
 */
exports.read = function (req, res) {
  res.json(req.pregunta);
};

/**
 * Update a pregunta
 */
exports.update = function (req, res) {
  var pregunta = req.pregunta;
  // console.log(req.pregunta);
  // console.log(req.body);

  pregunta.title = req.body.title;
  pregunta.description = req.body.description;
  pregunta.time = req.body.time;
  pregunta.order = req.body.order;
  pregunta.options = req.body.options;
  pregunta.imageURL = req.body.imageURL;

  pregunta.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      Pregunta.getPreguntasByOrden(function (err, result) {
        if (!err) {
          server_state.setListaPreguntas(result);
          res.json(pregunta);
        }else {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        }

      });
    }
  });
};

/**
 * Delete an pregunta
 */
exports.delete = function (req, res) {
  var pregunta = req.pregunta;

  pregunta.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      Pregunta.getPreguntasByOrden(function (err, result) {
        if (!err) {
          server_state.setListaPreguntas(result);
          res.json(pregunta);
        }else {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        }

      });
    }
  });
};

/**
 * List of Preguntas
 */
exports.list = function (req, res) {
  Pregunta.find().sort({ order: 1 }).populate('user', 'displayName').exec(function (err, preguntas) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.status(200).json(preguntas);
    }
  });
};

/**
 * Pregunta middleware
 */
exports.preguntaByID = function (req, res, next, id) {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Pregunta is invalid'
    });
  }

  Pregunta.findById(id).populate('user', 'displayName').exec(function (err, pregunta) {
    if (err) {
      return next(err);
    } else if (!pregunta) {
      return res.status(404).send({
        message: 'No pregunta with that identifier has been found'
      });
    }
    req.pregunta = pregunta;
    next();
  });
};

/**
 * Update profile picture
 */
exports.changePicture = function (req, res) {
  // console.log(req);
  console.log('Subiendo imagen');
  // var pregunta = req.pregunta;
  var message = null;
  var upload = multer(config.uploads.picturePreguntas).single('newPicture');
  var profileUploadFileFilter = require(path.resolve('./config/lib/multer')).profileUploadFileFilter;
  
  // Filtering to upload only images
  upload.fileFilter = profileUploadFileFilter;

  if (req.user.roles.indexOf('admin') !== -1) {
    upload(req, res, function (uploadError) {
      if(uploadError) {
        return res.status(400).send({
          message: 'Error occurred while uploading picture'
        });
      } else {
        res.status(200).send({ 'imageURL': config.uploads.picturePreguntas.dest + req.file.filename });
        // pregunta.imageURL = config.uploads.picturePreguntas.dest + req.file.filename;

        // pregunta.save(function (saveError) {
        //   if (saveError) {
        //     return res.status(400).send({
        //       message: errorHandler.getErrorMessage(saveError)
        //     });
        //   } else {
        //     res.status(200).json(pregunta);
        //     // req.login(user, function (err) {
        //     //   if (err) {
        //     //     res.status(400).send(err);
        //     //   } else {
        //     //     res.json(user);
        //     //   }
        //     // });
        //   }
        // });
      }
    });
  } else {
    res.status(400).send({
      message: 'No eres admin para realizar esta accion'
    });
  }
};
