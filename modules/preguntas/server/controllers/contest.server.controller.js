'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Contest = mongoose.model('Contest'),
  multer = require('multer'),
    moment = require('moment'),
  config = require(path.resolve('./config/config')),
    server= require(path.resolve('./config/lib/socket.io')),
    Pregunta = mongoose.model('Pregunta'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));


var server_state = require(path.resolve('./config/lib/server_state'))();
/**
 * Update or inserta a contest
 */
exports.readDate = function (req, res) {
  res.json({'date':moment(server_state.getDate()).format('DD/MM/YYYY HH:mm')});
};

exports.updateDate = function (req, res) {
  server_state.changeDate(req.body.date);
  res.json({'date': server_state.getDate()});
};

exports.restart = function (req, res) {
  server_state.restartServer();
  Pregunta.removeAnswers(function(err, obj){
    if(err){
      console.log(err);
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      solaci_server.createTimerWaitContest();
      res.json({'success': true});
    }
  })

};

/**
 * Contest data
 */
exports.list = function (req, res) {
  var Pregunta = mongoose.model('Pregunta');
  //Pregunta.insertRespuesta('56ec0149c8eb23887dc9218a',{option:'56ec0149c8eb23887dc9218c', user:"56ed5f63c0e689fe7a357856", time:65}, function(err, obj){
  //Pregunta.getCantRespuestasByUsuario("56efff23a5c9ca7919213834", true, function(err, obj){
  //Pregunta.getCantPreguntasSinRespuestasByUsuario("56efff23a5c9ca7919213834",  function(err, obj){
  //Pregunta.getTiempoPromedioTodasRespuestasByUsuario("56efff23a5c9ca7919213834",  function(err, obj){
  //Pregunta.getEstadisticasUsuario("56efff23a5c9ca7919213834",  function(err, obj){
  // Pregunta.getEstadisticasUsuarios(function(err, obj){
  // Pregunta.removeAnswers(function(err, obj){
  //Pregunta.getWinner(function (err, obj) {
    //Pregunta.getOpcionByPreguntaUsuario('56ec04d9a50e89a57fc6dc39','56e8736978dbf8d3450fcbf0', function(err, obj){
    //Pregunta.getPreguntasByOrden(function(err, obj){
    //Pregunta.getTiempoPromedioRespuestas(true,function(err, obj){
    //Pregunta.getRespuestasByPregunta('56ec04d9a50e89a57fc6dc39',function(err, obj){
    //Pregunta.getCantRespuestasByPregunta('56ec04d9a50e89a57fc6dc39', false,function(err, obj){
    //Pregunta.getTiempoPromedioRespuestasByPregunta('56ec04d9a50e89a57fc6dc39', false,function(err, obj){
    //Pregunta.getTiempoPromedioTodasRespuestas(function(err, obj){
    //Pregunta.getEstadisticas('56ec04d9a50e89a57fc6dc39',function(err, obj){
    //Pregunta.getCantActivadosSinRespuestasByPregunta('56ec04d9a50e89a57fc6dc39',function(err, obj){
    //Pregunta.getEstadisticas('56ec04d9a50e89a57fc6dc39',function(err, obj){
    //Pregunta.getEstadisticasFinales(function(err, obj){
    //Pregunta.getWinner(function(err, obj){
    //Pregunta.getPreguntaModerador('56ec04d9a50e89a57fc6dc39',function(err, obj){
  //   console.log(err);
  //   if (err) {
  //     return res.status(400).send({
  //       message: errorHandler.getErrorMessage(err)
  //     });
  //   } else {
  //     var arr = [];
  //     var arr2 = [];
  //     for(var key of obj.keys()){
  //       arr[key] =  obj.get(key).correct;
  //       arr2[key] = obj.get(key).time;
  //     }
  //     res.status(200).json({correctos:arr, time:arr2});
  //   }
  // });

  //Contest.find().exec(function (err, contest) {
  //    if (err) {
  //        return res.status(400).send({
  //            message: errorHandler.getErrorMessage(err)
  //        });
  //    } else {
  //        res.json(contest);
  //    }
  //});
};
