'use strict';

// Configuring the Concurso module
angular.module('concurso').run(['Menus', '$filter',
  function (Menus, $filter) {
    var $translate = $filter('translate');
    // Set top bar menu items
    Menus.addMenuItem('topbar', {
      title: $translate('Contest'),
      state: 'concurso',
      roles: ['concursante']
    });
  }
]);
