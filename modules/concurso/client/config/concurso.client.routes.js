'use strict';

// Configure the 'concurso' module routes
angular.module('concurso').config(['$stateProvider',
  function ($stateProvider) {
    $stateProvider
      .state('concurso', {
        url: '/concurso',
        templateUrl: 'modules/concurso/client/views/concurso.client.view.html',
        data: {
          roles: ['concursante']
        }
      });
  }
]);
