'use strict';

// Create the 'concurso' controller
angular.module('concurso').controller('ConcursoController', ['$scope', '$location', 'Authentication', '$interval', 'Socket', 'SweetAlert', '$filter',
  function ($scope, $location, Authentication, $interval, Socket, SweetAlert, $filter) {
    $scope.user = Authentication.user;
    var $translate = $filter('translate');
    // If user is not signed in then redirect back home
    if (!Authentication.user) {
      $location.path('/');
    }

    // Make sure the Socket is connected
    // if (!Socket.socket) {
    //   Socket.connect();
    // }
    Socket.connect();

    // Las variables internas del modulo
    // $scope.contest_status = null;
    // $scope.now_date = new Date('Mar 22 2016 16:21:00');
    $scope.haVotado = null;
    $scope.pregunta = {
      created: 'Wed Mar 16 2016 17:11:54 GMT-0400 (EDT)',
      title: 'Finca el Abra 2',
      description: 'Finca el Abra',
      imageURL: './modules/preguntas/client/img/bd83683ba488d13b3350272770d08264',
      order: 1,
      options: [{
        _id: '56e9cc1ac5a5d0fa52d1f4cd',
        name: 'Finca el Abra'
      }, {
        _id: '56e9cc1ac5a5d0fa52d1f4cc',
        name: 'Isal de la juventud'
      }, {
        _id: '56e9cc1ac5a5d0fa52d1f4cc',
        name: 'Santiago'
      }, {
        _id: '56e9cc1ac5a5d0fa52d1f4cc',
        name: 'Camaguey'
      }],
      answers: [],
      __v: 0,
      time: 120,
      _id: '56e9cc1ac5a5d0fa52d1f4cb'
    };

    // Forma un mensaje para enviar la respuesta del usuario a una pregunta
    $scope.responderPregunta = function (option) {
      if (!$scope.haVotado) {
        SweetAlert.swal({
          title: $translate('Are_You_Sure'),
          text: $translate('You_Have_Chosen') + ': ' + option.name,
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#5cb85c',
          confirmButtonText: $translate('Yes_I_Want_Vote'),
          cancelButtonText: $translate('NO'),
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function (isConfirm) {
          if (isConfirm) {
            Socket.emit('msg-answer-question', {
              uid: $scope.user._id,
              qid: $scope.pregunta._id,
              oid: option._id
            });
            $scope.haVotado = true;
            $scope.option_selected = option._id;
          }
        });
      }
    };

    // Genera numeros aleatorios para las opciones y mostrarlas en diferentes posiciones en cada navegador
    $scope.cambiarOrdenOpciones = function () {
      for (var i = $scope.pregunta.options.length - 1; i >= 0; i--) {
        $scope.pregunta.options[i].position = Math.random();
      }
    };

    // Funcion para calcular el timpo restante para empezar el concurso
    $scope.countDownContestNuevo = function (t) {
      var segundos = (t / 1000);
      if (t > 0) {
        // $scope.days = Math.floor(t / (86400000));
        $scope.hours = Math.floor((t / 3600000) % 24);
        $scope.minutes = Math.floor((segundos / 60) % 60);
        $scope.seconds = Math.floor(segundos % 60);
      } else {
        $scope.seconds = 0;
        $scope.minutes = 0;
        $scope.hours = 0;
        // $scope.days = 0;
      }
    };

    // Grafico para los segundos
    $scope.kSeconds = {
      displayPrevious: true,
      barCap: 25,
      trackWidth: 10,
      barWidth: 10,
      max: 60,
      trackColor: 'rgba(255,0,0,.1)',
      prevBarColor: 'rgba(0,0,0,.2)',
      textColor: 'rgba(255,0,0,.6)',
      size: 150,
      unit: ' s',
      readOnly: true,
      scale: {
        enabled: true,
        type: 'lines',
        color: 'gray',
        width: 1,
        quantity: 12,
        height: 8
      }
    };

    // Grafico para los minutos
    $scope.kMinutes = {
      displayPrevious: true,
      barCap: 25,
      trackWidth: 10,
      barWidth: 10,
      max: 60,
      trackColor: 'rgba(255,0,0,.1)',
      prevBarColor: 'rgba(0,0,0,.2)',
      textColor: 'rgba(255,0,0,.6)',
      size: 150,
      unit: ' m',
      readOnly: true,
      scale: {
        enabled: true,
        type: 'lines',
        color: 'gray',
        width: 1,
        quantity: 12,
        height: 8
      }
    };

    // Grafico para las horas
    $scope.kHours = {
      displayPrevious: true,
      barCap: 25,
      trackWidth: 10,
      barWidth: 10,
      max: 60,
      trackColor: 'rgba(255,0,0,.1)',
      prevBarColor: 'rgba(0,0,0,.2)',
      textColor: 'rgba(255,0,0,.6)',
      size: 150,
      unit: ' h',
      readOnly: true,
      scale: {
        enabled: true,
        type: 'lines',
        color: 'gray',
        width: 1,
        quantity: 12,
        height: 8
      }
    };

    //////
    // ClientSockets Solaci - Concursante
    //////
    var random = function randomIntInc(low, high) {
      return Math.floor(Math.random() * (high - low + 1) + low);
    };

    var timer_general = 0;

    // Timers || Actualiza el tiempo para empezar el concurso
    Socket.on('msg-update-timer-contest', function (message) {
      // message:
      // -fecha_concurso
      // -fecha_actual
      // -diferencia
      // $scope.contest_start_date = new Date(message.fecha_concurso);
      // $scope.diferencia = message.diferencia;
      if ($scope.interval)
        $interval.cancel($scope.interval);
      $scope.count = 1000;
      $scope.interval = $interval(function () {
        $scope.count += 1000;
        $scope.countDownContestNuevo(message.diferencia - $scope.count);
      }, 1000);
      console.log('Actualizando el tiempo del concurso');
      // console.log(message);
    });

    // SweetAlert.error('Preuba', 'Prueba');
    // SweetAlert.close();

    // Timers || Actualiza el tiempo de la pregunta actual
    Socket.on('msg-update-timer-question', function (message) {
      // message:
      // -fecha_actual
      // -fecha_final
      // -diferencia
      $scope.time_question = Math.floor(message.diferencia / 1000);
      if ($scope.countDownTimeQuestion) {
        // Ya ha sido programado un intervalo para reducir el tiempo de la pregunta actual
        $interval.cancel($scope.countDownTimeQuestion);
      }
      $scope.countDownTimeQuestion = $interval(function () {
        if ($scope.time_question > 0) {
          $scope.time_question--;
        } else {
          if (!$scope.haVotado) {
            SweetAlert.swal({
              title: $translate('Time_Finish'),
              text: $translate('You_Can_Not_Vote'),
              type: 'error',
              confirmButtonText: $translate('Accept'),
              timer: 3000
            });
          }
          $interval.cancel($scope.countDownTimeQuestion);
        }
      }, 1000);
      console.log('Actualizando el tiempo de la pregunta');
      // console.log(message);
    });

    // Wait Contest || Actualiza el tiempo para empezar el concurso
    Socket.on('msg-wait-start-contest', function (message) {
      // message:
      // -fecha_concurso
      // -fecha_actual
      // -diferencia
      // $scope.contest_start_date = new Date(message.fecha_concurso);
      // $scope.diferencia = message.diferencia;
      // $scope.reducirContestStart();
      if ($scope.interval)
        $interval.cancel($scope.interval);
      $scope.count = 1000;
      $scope.interval = $interval(function () {
        $scope.count += 1000;
        $scope.countDownContestNuevo(message.diferencia - $scope.count);
      }, 1000);
      // Pongo el estado del concurso en espera y se muestra el contador regresivo
      $scope.contest_status = 'wait';
      console.log('Se espera por comienzo del concurso...');
    });

    // Start Contest || El concurso ha iniciado y el concursante debe prestar atencion al moredador
    Socket.on('msg-start-contest-concursante', function (message) {
      // console.log(message);
      // message:
      // -{}

      // El concursante muestra pantalla de bienvenida
      // y atención al moderador

      // Cambio el estado del concurso para decirle al usuario que debe presetar atencion al moderador
      $scope.contest_status = 'start_contest';
      console.log('Concursante: comienza el concurso y atento a la presentación...');
    });

    // Start Question || Empieza una pregunta
    Socket.on('msg-start-question-concursante', function (message) {
      // console.log(message);
      // message:
      // -qid
      // -qname
      // -options
      // -time
      // -status: 0 - sin contestar aún, 1 - ya contestó
      // -oid: si status = 1 es la opción que contestó
      // $scope.haVotado = false;
      $scope.pregunta._id = message.qid;
      $scope.pregunta.title = message.qname;
      $scope.pregunta.options = message.options;
      $scope.pregunta.time = Math.floor(message.time / 1000);
      $scope.haVotado = message.status;
      $scope.option_selected = message.oid;
      $scope.time_question = Math.floor(message.time / 1000);

      // Pregunto si ya ha votado para habilitar o activar las opciones
      if ($scope.haVotado) {
        $scope.contest_status = 'start_question';
        $scope.haVotado = true;
        $scope.cambiarOrdenOpciones();
        // angular.forEach($scope.pregunta.options, function(option, key) {
        //   if (option._id === $scope.option_selected) {
        //     $scope.pregunta.options[key].
        //   }56f199fd7ebf17ce48afde6c
        // });
      } else {
        // Cambio el estado para que el usuario pueda responder la pregunta y cambio el orden de las opciones
        $scope.contest_status = 'start_question';
        $scope.cambiarOrdenOpciones();
      }
      if ($scope.countDownTimeQuestion) {
        // Ya ha sido programado un intervalo para reducir el tiempo de la pregunta actual
        $interval.cancel($scope.countDownTimeQuestion);
      }
      $scope.countDownTimeQuestion = $interval(function () {
        if ($scope.time_question > 0) {
          $scope.time_question--;
        } else {
          if (!$scope.haVotado) {
            SweetAlert.swal({
              title: $translate('Time_Finish'),
              text: $translate('You_Can_Not_Vote'),
              type: 'error',
              confirmButtonText: $translate('Accept'),
              timer: 3000
            });
          }
          $interval.cancel($scope.countDownTimeQuestion);
        }
      }, 1000);
      // Wait for responde the question
      console.log('Concursante: debe responder una nueva pregunta...');
    });

    // Start Moderator
    Socket.on('msg-wait-moderation-concursante', function (message) {
      // console.log(message);
      // message:
      // -qid
      // -qname
      // -option-correct
      // -time-answered, > 0: contesto, -1: se abstuvo
      // -correct: true/false
      // Cierra el modal abierto en este momento
      // SweetAlert.close();
      $scope.pregunta._id = message.qid;
      $scope.pregunta.title = message.qname;
      // $scope.pregunta.options = message.options;
      $scope.time_answered = (message.time_answered / 1000).toFixed(3);
      // $scope.pregunta.time = message.time;
      // $scope.haVotado = message.status;
      $scope.option_correct = message.option_correct;
      $scope.option_answered = message.option_answered;
      if (message.time_answered <= 0) {
        $scope.time_answered = 0;
        // $scope.option_correct = false;
        $scope.msg_votacion = {
          option: 3,
          name: $translate('Abstencion')
        };
        // Modifico la opcion elegida para que al usuario le aparezca "No votada"
        $scope.option_answered = {
          name: 'No votaste.'
        };
      } else if ($scope.option_answered.correct) {
        $scope.msg_votacion = {
          option: 1,
          name: $translate('You_Answer_OK')
        };
      } else {
        $scope.msg_votacion = {
          option: 2,
          name: $translate('You_Answer_Wrong')
        };
      }

      // Cambio el estado a esperando moderacion y muestro la opcion que eligio el usuario
      $scope.contest_status = 'wait_moderation';

      // Wait for moderator explain the question
      // if (timer_general){
      //   clearInterval(timer_general);
      // }
      console.log('Concursante: en espera de explicación de pregunta...');
    });

    // End Questions
    Socket.on('msg-end-questions-concursante', function (message) {
      // message:
      // - correctas
      // - incorrectas
      // - abstenciones
      // - tiempo_promedio
      // - tiempo_total
      $scope.user_stats = {
        correctas: message.respuestas_correctas,
        incorrectas: message.respuestas_incorrectas,
        abstenciones: message.abstenciones,
        tiempo_promedio: (message.tiempo_promedio / 1000).toFixed(3),
        tiempo_total: ((message.tiempo_promedio * message.preguntas) / 1000).toFixed(3)
      };
      // Cambio el estado del concurso para mostrar las estadisitcas
      $scope.contest_status = 'end_questions';
      // console.log(message);
      console.log('...se agotaron las preguntas del concurso...');
    });

    // End Contest
    Socket.on('msg-end-contest-concursante', function (message) {
      // console.log(message);
      // message:
      // - {}

      // Cambio el estado del concurso para mostrar el fin del concurso
      $scope.contest_status = 'end_contest';
      console.log('Concursante: fin del concurso...');
    });
  }
]);
