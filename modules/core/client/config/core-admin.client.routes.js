'use strict';

// Setting up route
angular.module('core.admin.routes').config(['$stateProvider', '$translateProvider', 'localStorageServiceProvider',
  function ($stateProvider, $translateProvider) {
    $stateProvider
      .state('admin', {
        abstract: true,
        url: '/admin',
        template: '<ui-view/>',
        data: {
          roles: ['admin']
        }
      });
    $translateProvider.useStaticFilesLoader({
      prefix: '/translations/trans_',
      suffix: '.json'
    });
    $translateProvider.useSanitizeValueStrategy('escaped');
    $translateProvider.preferredLanguage('es');
  }
]);
