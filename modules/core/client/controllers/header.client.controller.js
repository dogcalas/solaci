'use strict';

angular.module('core').controller('HeaderController', ['$scope', '$state', 'Authentication', 'Menus', '$translate', 'localStorageService',
  function ($scope, $state, Authentication, Menus, $translate, localStorageService) {
    // Expose view variables
    $scope.$state = $state;
    $scope.authentication = Authentication;

    var value = localStorageService.get('idioma_active');
    if (value !== null) {
      $translate.use(value);
    } else {
      localStorageService.add('idioma_active', 'es');
    }

    $scope.changeLanguage = function(key) {
      localStorageService.add('idioma_active', key);
      $translate.use(key);
    };

    // console.log($state);
    // $scope.presentracion = false;
    // $scope.dondeVa = function(item) {
    //   // console.log(item);
    //   if (item == 'presentacion') {
    //     console.log('pre')
    //     $scope.presentracion = true;
    //   } else {
    //     $scope.presentracion = false;
    //   }
    // }

    // Get the topbar menu
    $scope.menu = Menus.getMenu('topbar');

    // Toggle the menu items
    $scope.isCollapsed = false;
    $scope.toggleCollapsibleMenu = function () {
      $scope.isCollapsed = !$scope.isCollapsed;
    };

    // Collapsing the menu after navigation
    $scope.$on('$stateChangeSuccess', function () {
      $scope.isCollapsed = false;
    });
  }
]);
