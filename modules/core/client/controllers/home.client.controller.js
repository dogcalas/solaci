'use strict';

angular.module('core').controller('HomeController', ['$scope', '$state', '$http', '$location', 'Authentication', 'Socket',
  function ($scope, $state, $http, $location, Authentication, Socket) {
    // This provides Authentication context.
    $scope.authentication = Authentication;
    Socket.connect();
    // console.log($scope.authentication);

    // Verifica si un usuario es concursante o no
    $scope.esConcursante = function () {
      // console.log($scope.authentication.user.roles.indexOf('moderador') !== -1 || $scope.authentication.user.roles.indexOf('admin') !== -1);
      if ($scope.authentication.user.roles) {
        if ($scope.authentication.user.roles.indexOf('concursante') < 0 && !($scope.authentication.user.roles.indexOf('moderador') !== -1 || $scope.authentication.user.roles.indexOf('admin') !== -1))
          return false;
        else
          return true;
      } else {
        // No hay usuario autenticado pero devuelvo true para que el visitante no vea el mensaje
        return true;
      }
    };

    // Obtiene los datos del usuario que esta autenticado actualmente
    $scope.obtenerDatos = function () {
      $http.get('/api/users/me').success(function (response) {
        if (response.roles.indexOf('concursante') !== -1) {
          // El usuario es concursante y lo redirecciono al concurso
          $scope.authentication.user = response;
          $state.go('concurso', {}, {
            reload: true,
            inherit: false,
            notify: true
          });
        }
        // console.log(response);
      }).error(function (response) {
        $scope.error = response.message;
      });
    };

    // Evita que la pagina principal escuche los mensajes
    $scope.destroySocket = function () {
      Socket.removeListener('msg-start-contest-concursante');
      Socket.removeListener('msg-update-timer-contest');
      Socket.removeListener('msg-update-timer-question');
      Socket.removeListener('msg-wait-start-contest');
      Socket.disconnect();
    };

    // Activated
    Socket.on('msg-user-activated', function (message) {
      // Aqui debo chequear el estado en que se encuentra el usuario y direccionarlo al concurso
      $scope.obtenerDatos();
      console.log('User Activated');
      $scope.destroySocket();
      // console.log(message);
    });
  }
]);
