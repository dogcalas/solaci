'use strict';

// Load the module dependencies
var
  mongoose = require('mongoose'),
  Contest = mongoose.model('Contest'),
  moment = require('moment');

require('moment-range');
require('humanize');

var universe = global || window;

exports = module.exports = AccessServer;

function AccessServer() {
  if (!(this instanceof AccessServer)) {
    var singleton = universe.access_server;
    return singleton ? singleton : new AccessServer();
  }

  if (universe.access_server) {
    throw 'El server no puede crearse dos veces';
  }

  universe.access_server = this;
}

AccessServer.prototype.getPreguntasByOrden = function () {
  var Pregunta = mongoose.model('Pregunta');
  var this_access = this;

  console.log('preguntas by order');
  Pregunta.getPreguntasByOrden(function (err, result) {
    if (!err) {
      var server_state = require('./server_state')();
      server_state.setListaPreguntas(result);
      if(server_state.concurso_moderacion){
        var pregunta = server_state.previus();
        if(pregunta) {
          this_access.getAnswerByQuestion(pregunta);
        }
      }
    } else {
      console.log(err);
    }
  });
};

AccessServer.prototype.getAnswerByQuestion = function (question) {
  var Pregunta = mongoose.model('Pregunta');

  var server_state = require('./server_state')();

  console.log('buscar respuestas');
  Pregunta.getRespuestasByPregunta(question._id,function(err, objects){
    if (!err) {
      objects.forEach(function(obj, idx, arr){
        server_state.registerAnswer(obj.user, question._id, obj.option, obj.time);
      });
    } else {
      console.log(err);
    }
  });
};
