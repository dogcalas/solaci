'use strict';

var events = require('events');
var mongoose = require('mongoose');
var moment = require('moment');

var UserSockets = require('./user_sockets');

require('moment-range');
require('humanize');

exports = module.exports = UsersConnection;

function UsersConnection() {
  if (!(this instanceof UsersConnection)) return new UsersConnection();

  events.EventEmitter.call(this);

  this.sockets = {}; // sid -> socket
  this.sockets_count = 0;

  this.users = {};		// uid -> user
  this.users_count = 0;

  this.administradores = {}; // uid -> user
  this.administradores_count = 0;

  this.moderadores = {};		// uid -> user
  this.moderadores_count = 0;

  this.concursantes = {};		// uid -> user
  this.concursantes_count = 0;
  this.max_concursantes = 0;

  this.user_statistis = {};
  this.user_statistis_count = 0;

  this.por_activar = {};		// uid -> user
  this.por_activar_count = 0;

  this.io = null;

  this.on('onSocketDisconnected', function (user, socket) {
    console.log('Socket for user: ', socket.request.user.username, ' disconnected!!!');
  });

  this.socketDisconnected = function (user, socket) {
    this.emit('onSocketDisconnected', user, socket);
  };
}

UsersConnection.prototype.activateUserConnection = function (uid) {
  var users_cnn = this;

  var users = users_cnn.users;
  var administradores = users_cnn.administradores;
  var moderadores = users_cnn.moderadores;
  var concursantes = users_cnn.concursantes;
  var por_activar = users_cnn.por_activar;

  var userSocket = por_activar[uid.toString()];
  console.log(userSocket);

  if (userSocket) {
    delete por_activar[uid.toString()];

    if (userSocket.user.roles.indexOf('admin') !== -1) {
      userSocket.joinRoom('administradores');
      if (!administradores[uid]) {
        administradores[uid] = users[uid];
        users_cnn.administradores_count++;
      }
      console.log('Administradores');
      users_cnn.initializeUsersConnected(uid);
    } else if (userSocket.user.roles.indexOf('moderador') !== -1) {
      userSocket.joinRoom('moderadores');
      userSocket.joinRoom('en.concurso');
      if (!moderadores[uid]) {
        moderadores[uid] = users[uid];
        users_cnn.moderadores_count++;
      }
      console.log('Moderadores');
      users_cnn.initializeUsersConnected(uid);
    } else if (userSocket.user.roles.indexOf('concursante') !== -1) {
      userSocket.joinRoom('concursantes');
      userSocket.joinRoom('en.concurso');

      if (!concursantes[uid]) {
        concursantes[uid] = users[uid];
        users_cnn.concursantes_count++;
      }
      console.log('Concursantes');
      userSocket.sendMessage('msg-user-activated', {});
      users_cnn.initializeUsersConnected(uid);
    } else if (userSocket.user.roles.indexOf('user') !== -1) {
      if (!por_activar[uid]) {
        por_activar[uid] = users[uid];
        users_cnn.por_activar_count++;
      }
    }
  }
};

UsersConnection.prototype.registerUserConnection = function (user, socket, io) {
  var users_cnn = this;

  this.io = io;

  var users = users_cnn.users;
  var sockets = users_cnn.sockets;
  var administradores = users_cnn.administradores;
  var moderadores = users_cnn.moderadores;
  var concursantes = users_cnn.concursantes;
  var por_activar = users_cnn.por_activar;

  var uid = user._id;

  console.log('Register socket(s)...');

  if (!users[uid]) {
    users[uid] = new UserSockets(uid, user);
    users_cnn.users_count++;
  }

  users[uid].addSocket(socket);

  if (user.roles.indexOf('admin') !== -1) {
    socket.join('administradores');
    if (!administradores[uid]) {
      administradores[uid] = users[uid];
      users_cnn.administradores_count++;
    }
  } else if (user.roles.indexOf('moderador') !== -1) {
    socket.join('moderadores');
    socket.join('en.concurso');
    if (!moderadores[uid]) {
      moderadores[uid] = users[uid];
      users_cnn.moderadores_count++;
    }
  } else if (user.roles.indexOf('concursante') !== -1) {
    socket.join('concursantes');
    socket.join('en.concurso');

    if (!concursantes[uid]) {
      concursantes[uid] = users[uid];
      users_cnn.concursantes_count++;
      users_cnn.max_concursantes = Math.max(users_cnn.max_concursantes, users_cnn.concursantes_count);
    }
  } else if (user.roles.indexOf('user') !== -1) {
    if (!por_activar[uid]) {
      por_activar[uid] = users[uid];
      users_cnn.por_activar_count++;
    }
  }



  sockets[socket.id] = users[uid];
  users_cnn.sockets_count++;

  console.log(
    'Socket(s): ', users_cnn.totalSocketsConnected(), '|',
    'User(s):', users_cnn.totalUsersConnected(), '|',
    'Moderator:', users_cnn.totalModeradoresSocketsConnected(), '|',
    'Concursante(s):', users_cnn.totalConcunsantesSocketsConnected(), '|',
    'Administrator(s):', users_cnn.totalAdminsSocketsConnected()
  );

  // Socket disconnected
  socket.on('disconnect', function () {
    users[uid].removeSocket(socket);
    if (!users[uid].count()) {
      delete users[uid];
      users_cnn.users_count--;
    }

    delete sockets[socket.id];
    users_cnn.sockets_count--;

    if (administradores[uid] && !administradores[uid].count()) {
      delete administradores[uid];
      users_cnn.administradores_count--;
    } else if (moderadores[uid] && !moderadores[uid].count()) {
      delete moderadores[uid];
      users_cnn.moderadores_count--;
    } else if (concursantes[uid] && !concursantes[uid].count()) {
      delete concursantes[uid];
      users_cnn.concursantes_count--;
    } else if (por_activar[uid] && !por_activar[uid].count()) {
      delete por_activar[uid];
      users_cnn.por_activar_count--;
    }

    users_cnn.socketDisconnected(users[uid], socket);
  });

  users_cnn.initializeUsersConnected(uid);

  return this;
};

UsersConnection.prototype.registerUserStatistics = function (uid, user_stats) {
  var users_cnn = this;

  if (user_stats) {
    users_cnn.user_statistis[uid] = {
      user: uid,
      stats: {
        'respuestas_correctas': user_stats.correct,
        'respuestas_incorrectas': user_stats.incorrect,
        'abstenciones': user_stats.abstenciones,
        'tiempo_total': user_stats.time,
        'tiempo_promedio': (user_stats.correct) ? (user_stats.time / user_stats.correct) : 0
      }
    };

    if (!users_cnn.user_statistis[uid]) {
      users_cnn.user_statistis_count++;
    }
  }

  return this;
};

UsersConnection.prototype.getUserStatistics = function (uid) {
  var users_cnn = this;
  var server_state = require('./server_state')();

  var user_stats = {
    user: uid,
    stats: {
      'respuestas_correctas': 0,
      'respuestas_incorrectas': 0,
      'abstenciones': server_state.lista_preguntas.length,
      'tiempo_total': 0,
      'tiempo_promedio': 0
    }
  };

  if (uid && users_cnn.user_statistis[uid]) {
    user_stats = users_cnn.user_statistis[uid];
  }

  return user_stats;
};

UsersConnection.prototype.initializeUsersConnected = function (uid, socket) {
  var users_cnn = this;
  var users = users_cnn.users;
  var moderadores = users_cnn.moderadores;
  var concursantes = users_cnn.concursantes;
  var user_socket = users[uid];
  var user = user_socket.user;
  var server_state = require('./server_state')();

  server_state.users_count = users_cnn.max_concursantes;

  var Pregunta = mongoose.model('Pregunta');

  if (concursantes[uid] || moderadores[uid]) {
    var mfactual = moment();
    var question = null;
    var nquestion = null;

    var status = server_state.getServerStatus();

    console.log('Usuario: ', user.username, 'inicializa en:');
    if (status === 1) {
      console.log('...espera del concurso');

      // Enviar a todos los usuarios conectados y autenticados que estan en espera
      mfactual = moment();
      var mfconcurso = moment(server_state.fecha_concurso);
      var dr = moment.range(mfactual, mfconcurso);
      var diff = dr.diff();
      user_socket.sendMessage('msg-wait-start-contest',
        {
          'fecha_concurso': mfconcurso.valueOf(),
          'fecha_actual': mfactual.valueOf(),
          'diferencia': diff
        });
    } else if (status === 2) {
      console.log('...presentación del concurso');

      if (moderadores[uid]) {
        // Enviar a concursantes
        user_socket.sendMessage('msg-start-contest-moderador', {
          'imagen_inicial': 'url/imagen.png',
          'descripcion': 'texto',
          'cantidad_preguntas': server_state.lista_preguntas.length
        });
      }

      if (concursantes[uid]) {
        // Enviar a concursantes
        user_socket.sendMessage('msg-start-contest-concursante', {});
      }

    } else if (status === 3) {
      console.log('...ronda de pregunta...');

      question = server_state.previus();
      nquestion = server_state.current();

      if (question) {
        console.log('...se asigna una nueva pregunta y se envía');
        var time = question.time;

        var startTime = server_state.getStartTime();
        var mffinal = startTime.valueOf() + (time * 1000);
        mfactual = moment().valueOf();
        var ltime = Math.floor(mffinal - mfactual);

        var opts = [];
        for (var c_opt of question.options) {
          opts.push({'_id': c_opt._id, 'name': c_opt.name});
        }

        var moawnser = {
          'qid': question._id,
          'qname': question.title,
          'options': opts,
          'time': ltime,
          'status': 0,
          'oid': ''
        };

        var uanswer = server_state.getRegisterAnswer(uid);

        if (uanswer) {
          moawnser.status = 1;
          moawnser.oid = uanswer.oid;
        }

        if (concursantes[uid]) {
          user_socket.sendMessage('msg-start-question-concursante', moawnser);
        }

        if (moderadores[uid]) {
          user_socket.sendMessage('msg-start-question-moderador', {
            'question': question,
            'next_question': nquestion,
            'time': ltime
          });
        }
      } else {
        // TODO
      }
    } else if (status === 4) {
      console.log('...moderación de pregunta');

      question = server_state.previus();
      nquestion = server_state.current();

      let cant_preguntas = server_state.lista_preguntas.length;
      let cant_concursantes = users_cnn.maxConcunsantesSocketsConnected();
      let pregunta_actual = server_state.pregunta_activa;

      if (question) {
        if (moderadores[uid]) {
          var opciones = server_state.getOptionStatistic();
          console.log(opciones);

          user_socket.sendMessage('msg-wait-moderation-moderador', {
            'question': question,
            'next_question': nquestion,
            'opciones': opciones
          });

          Pregunta.getEstadisticas(
            question._id,
            function (err, obj) {
              if (!err) {
                user_socket.sendMessage('msg-update-statistics-question-moderador', {
                  'cant_usuarios_activados': cant_concursantes,
                  'preguntas': cant_preguntas,
                  'respuestas_correctas': obj.respuestas_correctas,
                  'respuestas_incorrectas': obj.respuestas_incorrectas,
                  'abstenciones':  cant_concursantes - (obj.respuestas_correctas + obj.respuestas_incorrectas),
                  'tiempo_promedio': obj.tiempo_promedio,
                  'question_id': question._id,
                  'pregunta_actual': pregunta_actual,
                  'opciones': opciones
                });
              }
            });
        }

        if (concursantes[uid]) {
          var crtoption = null;

          question.options.forEach(function (opt, idx, arr) {
            if (opt.correct) {
              crtoption = opt;
              return false;
            }
          });

          var msg_std_awnser = {
            'qid': question._id,
            'qname': question.title,
            'option_correct': crtoption,
            'time_answered': -1,
            'correct': false,
            'option_answered': null
          };

          var muanswer = server_state.getRegisterAnswer(uid);

          if (muanswer) {
            var opanwser = null;

            for (var opt of question.options) {
              if (opt._id.toString() === muanswer.oid.toString()) {
                opanwser = opt;
                break;
              }
            }

            msg_std_awnser.time_answered = muanswer.time;
            msg_std_awnser.option_answered = opanwser;
          }

          user_socket.sendMessage('msg-wait-moderation-concursante', msg_std_awnser);
        }
      }
    } else if (status === 5) {
      console.log('...presentación de las estadísticas');

      let cant_preguntas = server_state.pregunta_activa;
      let cant_concursantes = users_cnn.maxConcunsantesSocketsConnected();

      if (concursantes[uid]) {
        let user_stats = users_cnn.getUserStatistics(uid);
        if (user_stats) {
          user_socket.sendMessage('msg-end-questions-concursante', {
            'cant_usuarios_activados': cant_concursantes,
            'preguntas': cant_preguntas,
            'respuestas_correctas': user_stats.stats.respuestas_correctas,
            'respuestas_incorrectas': user_stats.stats.respuestas_incorrectas,
            'abstenciones': (cant_preguntas - (user_stats.stats.respuestas_correctas + user_stats.stats.respuestas_incorrectas)),
            'tiempo_total': user_stats.stats.tiempo_total,
            'tiempo_promedio': user_stats.stats.tiempo_promedio
          });
        }

        //Pregunta.getEstadisticasUsuario(
        //  uid,
        //  function (err, obj) {
        //    if (!err) {
        //      user_socket.sendMessage('msg-end-questions-concursante', {
        //        'cant_usuarios_activados': obj.cant_usuarios_activados,
        //        'preguntas': obj.preguntas,
        //        'respuestas_correctas': obj.respuestas_correctas,
        //        'respuestas_incorrectas': obj.respuestas_incorrectas,
        //        'abstenciones': obj.abstenciones,
        //        'tiempo_promedio': obj.tiempo_promedio
        //      });
        //    }
        //  }
        //);
      }

      if (moderadores[uid]) {
        Pregunta.getEstadisticasPreguntas(
          function (err_est, questions) {
            if (!err_est) {
              Pregunta.getWinner(function (err, user) {
                if (!err) {
                  questions.forEach(function (value) {
                    //value.abstenciones = cant_concursantes - value.abstenciones;
                    value.abstenciones = (cant_concursantes)?(cant_concursantes - value.abstenciones):0;
                  });

                  user_socket.sendMessage('msg-update-statistics-contest-moderador', {
                    'estadisticas_preguntas': questions
                  });

                  user_socket.sendMessage('msg-end-questions-moderador', {
                    'estadisticas_preguntas': questions,
                    'user_win': user
                  });
                }
              });
            }
          }
        );

        //Pregunta.getEstadisticasFinales(
        //  function (err_est, obj) {
        //    if (!err_est) {
        //      Pregunta.getWinner(function (err, user) {
        //        if (!err) {
        //          user_socket.sendMessage('msg-update-statistics-contest-moderador', {
        //            'cant_usuarios_activados': cant_concursantes,
        //            'preguntas': cant_preguntas,
        //            'respuestas_correctas': obj.respuestas_correctas,
        //            'respuestas_incorrectas': obj.respuestas_incorrectas,
        //            'abstenciones': obj.abstenciones,
        //            'tiempo_promedio': obj.tiempo_promedio,
        //            'pregunta_actual': cant_preguntas
        //          });
        //
        //          user_socket.sendMessage('msg-end-questions-moderador', {
        //            'cant_usuarios_activados': cant_concursantes,
        //            'preguntas': cant_preguntas,
        //            'respuestas_correctas': obj.respuestas_correctas,
        //            'respuestas_incorrectas': obj.respuestas_incorrectas,
        //            'abstenciones': obj.abstenciones,
        //            'tiempo_promedio': obj.tiempo_promedio,
        //            'pregunta_actual': cant_preguntas,
        //            'user_win': user
        //          });
        //        }
        //      });
        //    }
        //  }
        //);
      }
    } else if (status === 6) {
      console.log('...presentación del ganador');
      let user_stats = null;

      let cant_preguntas = server_state.pregunta_activa;
      let cant_concursantes = users_cnn.maxConcunsantesSocketsConnected();

      if (concursantes[uid]) {
        user_stats = users_cnn.getUserStatistics(uid);
        if (user_stats) {
          user_socket.sendMessage('msg-end-questions-concursante', {
            'cant_usuarios_activados': cant_concursantes,
            'preguntas': cant_preguntas,
            'respuestas_correctas': user_stats.stats.respuestas_correctas,
            'respuestas_incorrectas': user_stats.stats.respuestas_incorrectas,
            'abstenciones': (cant_preguntas - (user_stats.stats.respuestas_correctas + user_stats.stats.respuestas_incorrectas)),
            'tiempo_total': user_stats.stats.tiempo_total,
            'tiempo_promedio': user_stats.stats.tiempo_promedio
          });
        }
      }

      // Pedir datos del ganador y enviarselo
      Pregunta.getWinner(
        function (err, user) {
          if (!err) {
            if (moderadores[uid]) {
              user_stats = users_cnn.getUserStatistics(user._id);
              if (user_stats) {
                user_socket.sendMessage('msg-winner-contest-presentacion', {
                  'user': user,
                  'cant_usuarios_activados': cant_concursantes,
                  'preguntas': cant_preguntas,
                  'respuestas_correctas': user_stats.stats.respuestas_correctas,
                  'respuestas_incorrectas': user_stats.stats.respuestas_incorrectas,
                  'abstenciones': (cant_preguntas - (user_stats.stats.respuestas_correctas + user_stats.stats.respuestas_incorrectas)),
                  //'abstenciones': user_stats.stats.abstenciones,
                  'tiempo_total': user_stats.stats.tiempo_total,
                  'tiempo_promedio': user_stats.stats.tiempo_promedio,
                  'pregunta_actual': cant_preguntas
                });

                user_socket.sendMessage('msg-winner-contest-moderador', {
                  'user': user,
                  'cant_usuarios_activados': cant_concursantes,
                  'preguntas': cant_preguntas,
                  'respuestas_correctas': user_stats.stats.respuestas_correctas,
                  'respuestas_incorrectas': user_stats.stats.respuestas_incorrectas,
                  'abstenciones': (cant_preguntas - (user_stats.stats.respuestas_correctas + user_stats.stats.respuestas_incorrectas)),
                  'tiempo_total': user_stats.stats.tiempo_total,
                  'tiempo_promedio': user_stats.stats.tiempo_promedio
                });
              }
            }

            //Pregunta.getEstadisticasUsuario(
            //  user._id,
            //  function (err_stat, user_stat) {
            //    if (!err_stat) {
            //      if(moderadores[uid]) {
            //        user_socket.sendMessage('msg-winner-contest-presentacion', {
            //          'user': user,
            //          'cant_usuarios_activados': user_stat.cant_usuarios_activados,
            //          'preguntas': user_stat.preguntas,
            //          'respuestas_correctas': user_stat.respuestas_correctas,
            //          'respuestas_incorrectas': user_stat.respuestas_incorrectas,
            //          'abstenciones': user_stat.abstenciones,
            //          'tiempo_promedio': user_stat.tiempo_promedio
            //        });
            //      }
            //
            //      if(concursantes[uid]) {
            //        user_socket.sendMessage('msg-winner-contest-concursante', {
            //          'user': user,
            //          'cant_usuarios_activados': user_stat.cant_usuarios_activados,
            //          'preguntas': user_stat.preguntas,
            //          'respuestas_correctas': user_stat.respuestas_correctas,
            //          'respuestas_incorrectas': user_stat.respuestas_incorrectas,
            //          'abstenciones': user_stat.abstenciones,
            //          'tiempo_promedio': user_stat.tiempo_promedio
            //        });
            //      }
            //    }
            //  }
            //);
          }
        });
    } else if (status === 7) {
      console.log('..final del concurso');
      if (moderadores[uid]) {
        user_socket.sendMessage('msg-end-contest-moderador', {});
      }

      if (concursantes[uid]) {
        user_socket.sendMessage('msg-end-contest-concursante', {});
      }
    }
  }

  return this;
};

UsersConnection.prototype.totalUsersConnected = function () {
  var users_cnn = this;

  return users_cnn.users_count;
};

UsersConnection.prototype.totalSocketsConnected = function () {
  var users_cnn = this;

  return users_cnn.sockets_count;
};

UsersConnection.prototype.totalConcunsantesSocketsConnected = function () {
  var users_cnn = this;

  return users_cnn.concursantes_count;
};

UsersConnection.prototype.maxConcunsantesSocketsConnected = function () {
  var users_cnn = this;

  return users_cnn.max_concursantes;
};

UsersConnection.prototype.totalModeradoresSocketsConnected = function () {
  var users_cnn = this;

  return users_cnn.moderadores_count;
};

UsersConnection.prototype.totalAdminsSocketsConnected = function () {
  var users_cnn = this;

  return users_cnn.administradores_count;
};

//UsersConnection.prototype.getConcursantes = function () {
//  return this.concursantes;
//};
//
//UsersConnection.prototype.getUserById = function (user_id) {
//  var users_cnn = this;
//
//  if (users_cnn.users[user_id])
//    return users_cnn.users[user_id];
//  else
//    return null;
//};

Object.setPrototypeOf(UsersConnection.prototype, events.EventEmitter.prototype);
