'use strict';

var moment = require('moment');

require('moment-range');

var universe = global || window;

// Define the Socket.io configuration method
exports = module.exports = ServerState;

function ServerState() {
  if (!(this instanceof ServerState)) {
    var singleton = universe.server_state; 
    return singleton ? singleton : new ServerState();
  }

  if (universe.server_state) {
    throw 'El server state no puede crearse dos veces';
  }

  universe.server_state = this;

  // persistent data
  this.fecha_concurso = null;
  this.concurso_activo = false;
  this.concurso_pregunta = false;
  this.concurso_moderacion = false;
  this.concurso_fin_preguntas = false;
  this.concurso_ganador = false;
  this.concurso_finalizado = false;
  this.users_count = 0;

  this.start_time = moment();
  this.pregunta_activa = 0;

  // transient data
  this.reg_preguntas = [];
  this.reg_options = [];
  this.lista_preguntas = [];
}

ServerState.prototype.getServerStatus = function () {
  var mfactual = moment();
  var mfconcurso = moment(this.fecha_concurso);
  var dr = moment.range(mfactual, mfconcurso);
  var diff = dr.diff();

  if (diff > 0) {
    // En espera de concurso...
    return 1;
  } else if (this.concurso_activo) {
    // Concurso activo...
    if (this.concurso_pregunta) {
      // Servidor inicializa en realizar pregunta...
      return 3;
    } else if (this.concurso_moderacion) {
      // Servidor inicializa en moderación de pregunta...
      return 4;
    } else if (this.concurso_fin_preguntas) {
      // Servidor inicializa en finalización de preguntas...
      return 5;
    } else if (this.concurso_ganador) {
      // Servidor inicializa en presentación del ganador...
      return 6;
    } else {
      //Servidor inicializa en presentacion del concurso...
      return 2;
    }
  } else if (this.concurso_finalizado) {
    //Servidor no realiza más acciones, fin del concurso...
    return 7;
  }
};

ServerState.prototype.setListaPreguntas = function (lista_preguntas) {
  this.lista_preguntas = lista_preguntas;
};

// start_time
ServerState.prototype.setStartTime = function (startTime) {
  this.start_time = startTime;
};

ServerState.prototype.getStartTime = function () {
  return this.start_time;
};

// Acciones de la máquina de estado
ServerState.prototype.esperarConcurso = function () {
  this.concurso_activo = false;
  this.concurso_pregunta = false;
  this.concurso_moderacion = false;
  this.concurso_fin_preguntas = false;
  this.concurso_ganador = false;
  this.concurso_finalizado = false;
  this.users_count = 0;
  this.reset();
};


// Acciones de la máquina de estado
ServerState.prototype.restartServer = function () {
  this.concurso_activo = true;
  this.concurso_pregunta = false;
  this.concurso_moderacion = false;
  this.concurso_fin_preguntas = false;
  this.concurso_ganador = false;
  this.concurso_finalizado = false;
  this.users_count = 0;
  this.reset();
  this.saveData();

};

ServerState.prototype.iniciarConcurso = function () {
  this.concurso_activo = true;
  this.concurso_pregunta = false;
  this.concurso_moderacion = false;
  this.concurso_fin_preguntas = false;
  this.concurso_ganador = false;
  this.concurso_finalizado = false;
  this.users_count = 0;
  this.reset();
};

ServerState.prototype.realizarPregunta = function () {
  this.concurso_activo = true;
  this.concurso_pregunta = true;
  this.concurso_moderacion = false;
  this.concurso_fin_preguntas = false;
  this.concurso_ganador = false;
  this.concurso_finalizado = false;
  this.reg_preguntas = [];
  this.reg_options = [];
};

ServerState.prototype.moderarPregunta = function () {
  this.concurso_activo = true;
  this.concurso_pregunta = false;
  this.concurso_moderacion = true;
  this.concurso_fin_preguntas = false;
  this.concurso_ganador = false;
  this.concurso_finalizado = false;
};

ServerState.prototype.finalizarPreguntas = function () {
  this.concurso_activo = true;
  this.concurso_pregunta = false;
  this.concurso_moderacion = false;
  this.concurso_fin_preguntas = true;
  this.concurso_ganador = false;
  this.concurso_finalizado = false;
};

ServerState.prototype.presentarGanador = function () {
  this.concurso_activo = true;
  this.concurso_pregunta = false;
  this.concurso_moderacion = false;
  this.concurso_fin_preguntas = false;
  this.concurso_ganador = true;
  this.concurso_finalizado = false;
};

ServerState.prototype.finalizarConcurso = function () {
  this.concurso_activo = false;
  this.concurso_pregunta = false;
  this.concurso_moderacion = false;
  this.concurso_fin_preguntas = false;
  this.concurso_ganador = false;
  this.concurso_finalizado = true;
};

ServerState.prototype.changeDate = function (date) {
  this.fecha_concurso =  moment(date, 'DD/MM/YYYY HH:mm');
  this.saveData();
};

ServerState.prototype.getDate = function () {
 return  this.fecha_concurso;
};

// Register Clients Answers
ServerState.prototype.registerAnswer = function (uid, qid, oid, time) {
  if (!this.reg_preguntas[uid.toString()]) {
    this.reg_preguntas[uid.toString()] = {
      'uid': uid,
      'qid': qid,
      'oid': oid,
      'time': time
    };
    if (this.reg_options[oid.toString()]) {
      this.reg_options[oid.toString()].count++;
    } else {
      let option = this.getOptionQuestion(qid, oid);
      if (option) {
        this.reg_options[oid.toString()] = {
          'name': option.name,
          'correct': option.correct,
          'count': 1
        };
      }
    }
  }
};

ServerState.prototype.getOptionStatistic = function () {
  var result = [];
  for (var key in this.reg_options){
    result.push(this.reg_options[key]);
  }
  return result;
};

ServerState.prototype.getOptionQuestion = function (qid, oid) {
  let pregunta = this.previus();
  let voption = null;

  if (pregunta && pregunta._id.toString() === qid.toString()) {
    pregunta.options.forEach(function (value) {
      if (oid.toString() === value._id.toString()) {
        voption = value;
        return value;
      }
    });
  }
  return voption;
};


ServerState.prototype.getRegisterAnswer = function (uid) {
  if (this.reg_preguntas[uid.toString()]) {
    return this.reg_preguntas[uid.toString()];
  } else {
    return false;
  }
};

ServerState.prototype.existUserAnswer = function (uid) {
  if (this.reg_preguntas[uid.toString()]) {
    return true;
  }
  return false;
};

// Iterate Questions
ServerState.prototype.reset = function () {
  this.pregunta_activa = 0;
};

ServerState.prototype.revert = function () {
  if (this.pregunta_activa > 0) {
    this.pregunta_activa--;
  }
};

ServerState.prototype.previus = function () {
  if (this.lista_preguntas !== null) {
    var idx = this.pregunta_activa - 1;
    if (idx >= 0) {
      return this.lista_preguntas[idx];
    }
  }
  return null;
};

ServerState.prototype.next = function () {
  if (this.lista_preguntas !== null) {
    if (this.pregunta_activa < this.lista_preguntas.length) {
      return this.lista_preguntas[this.pregunta_activa++];
    }
  }
  return null;
};

ServerState.prototype.current = function () {
  if (this.lista_preguntas !== null) {
    if (this.pregunta_activa < this.lista_preguntas.length) {
      return this.lista_preguntas[this.pregunta_activa];
    }
  }
  return null;
};

// Persist JSON Config
var jsonfile = require('jsonfile');

var fs = require('fs');
var path = require('path');

var mkdirSync = function (path) {
  try {
    fs.mkdirSync(path);
  } catch (e) {
    if (e.code !== 'EEXIST') throw e;
  }
};

function fileExists(filePath) {
  try {
    var result = fs.statSync(filePath).isFile();
    return result;
  }
  catch (err) {
    return false;
  }
}

ServerState.prototype.loadData = function () {
  if (!fileExists('data/data_config.json')) {
    mkdirSync('data');
    this.fecha_concurso = moment('06/10/2016 00:55', 'DD/MM/YYYY HH:mm');
    this.pregunta_activa = 0;
    this.concurso_activo = false;
    this.concurso_pregunta = false;
    this.concurso_moderacion = false;
    this.concurso_fin_preguntas = false;
    this.concurso_ganador = false;
    this.concurso_finalizado = false;
    this.users_count = 0;
    this.saveData();
  } else {
    var jsonData = jsonfile.readFileSync('data/data_config.json');
    if (jsonData) {
      this.fecha_concurso = moment(jsonData.fecha_concurso, 'DD/MM/YYYY HH:mm');
      this.pregunta_activa = jsonData.pregunta_activa || 0;
      this.concurso_activo = jsonData.concurso_activo || false;
      this.concurso_pregunta = jsonData.concurso_pregunta || false;
      this.concurso_moderacion = jsonData.concurso_moderacion || false;
      this.concurso_fin_preguntas = jsonData.concurso_fin_preguntas || false;
      this.concurso_ganador = jsonData.concurso_ganador || false;
      this.concurso_finalizado = jsonData.concurso_finalizado || false;
      this.users_count = jsonData.concurso_usuarios || 0;

      var server_state = this;
      setInterval(function () {
        server_state.saveData();
      }, 5000);
      console.log('Server state loaded ...');
    } else {
      console.log('Error reading server configuration');
    }
  }
};

ServerState.prototype.saveData = function () {
  var server_state = this;

  var jsonData = {
    'fecha_concurso': moment(server_state.fecha_concurso).format('DD/MM/YYYY HH:mm'),
    'pregunta_activa': server_state.pregunta_activa,
    'concurso_activo': server_state.concurso_activo,
    'concurso_pregunta': server_state.concurso_pregunta,
    'concurso_moderacion': server_state.concurso_moderacion,
    'concurso_fin_preguntas': server_state.concurso_fin_preguntas,
    'concurso_ganador': server_state.concurso_ganador,
    'concurso_finalizado': server_state.concurso_finalizado,
    'concurso_usuarios': server_state.users_count
  };

  jsonfile.writeFile('data/data_config.json', jsonData, function (err) {
    if (err) {
      console.log(err.message);
    }
  });
};
