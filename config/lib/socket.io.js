'use strict';

// Load the module dependencies
var config = require('../config'),
  path = require('path'),
  fs = require('fs'),
  http = require('http'),
  https = require('https'),
  cookieParser = require('cookie-parser'),
  passport = require('passport'),
  socketio = require('socket.io'),
  session = require('express-session'),
  MongoStore = require('connect-mongo')(session),
  UsersConnection = require('./users_connection'),
  moment = require('moment'),
  mongoose = require('mongoose');

require('moment-range');
require('humanize');

var universe = global || window;

// Define the Socket.io configuration method
exports = module.exports = SolaciServer;

function SolaciServer() {
  if (!(this instanceof SolaciServer)) {
    var singvaron = universe.solaci_server;
    return singvaron ? singvaron : new SolaciServer();
  }

  if (universe.solaci_server) {
    throw 'El server no puede crearse dos veces';
  }

  universe.solaci_server = this;

  require('events').EventEmitter.prototype._maxListeners = 50;

  this.REFRESH_TIME = 15000; // 5 sec
  this.timer_general = 0;
  this.timer_tiempo = 0;

  this.io = null;
  this.db = null;
  this.users_cnn = new UsersConnection();

  this.startTime = moment();
  this.endTime = moment();

  var users_cnn = this.users_cnn;
  users_cnn.on('onSocketDisconnected', function (user, socket) {
    console.log(
      'Socket(s): ', users_cnn.totalSocketsConnected(), '|',
      'User(s):', users_cnn.totalUsersConnected(), '|',
      'Moderator:', users_cnn.totalModeradoresSocketsConnected(), '|',
      'Concursante(s):', users_cnn.totalConcunsantesSocketsConnected(), '|',
      'Administrator(s):', users_cnn.totalAdminsSocketsConnected()
    );
  });

  // var this_server = this;
}

SolaciServer.prototype.createServer = function (app, db) {
  var server;
  var this_server = this;

  if (config.secure && config.secure.ssl === true) {
    // Load SSL key and certificate
    var privateKey = fs.readFileSync(path.resolve(config.secure.privateKey), 'utf8');
    var certificate = fs.readFileSync(path.resolve(config.secure.certificate), 'utf8');
    var options = {
      key: privateKey,
      cert: certificate,
      //  requestCert : true,
      //  rejectUnauthorized : true,
      secureProtocol: 'TLSv1_method',
      ciphers: [
        'ECDHE-RSA-AES128-GCM-SHA256',
        'ECDHE-ECDSA-AES128-GCM-SHA256',
        'ECDHE-RSA-AES256-GCM-SHA384',
        'ECDHE-ECDSA-AES256-GCM-SHA384',
        'DHE-RSA-AES128-GCM-SHA256',
        'ECDHE-RSA-AES128-SHA256',
        'DHE-RSA-AES128-SHA256',
        'ECDHE-RSA-AES256-SHA384',
        'DHE-RSA-AES256-SHA384',
        'ECDHE-RSA-AES256-SHA256',
        'DHE-RSA-AES256-SHA256',
        'HIGH',
        '!aNULL',
        '!eNULL',
        '!EXPORT',
        '!DES',
        '!RC4',
        '!MD5',
        '!PSK',
        '!SRP',
        '!CAMELLIA'
      ].join(':'),
      honorCipherOrder: true
    };

    // Create new HTTPS Server
    server = https.createServer(options, app);
  } else {
    // Create a new HTTP server
    server = http.createServer(app);
  }

  // Server State
  this.server_state = require('./server_state')();
  this.server_state.loadData();

  // Server access
  var server_access = new require('./server_access')();
  server_access.getPreguntasByOrden();

  // Load Statistics
  var Pregunta = mongoose.model('Pregunta');
  Pregunta.getEstadisticasUsuarios(
    function (err, users) {
      if (!err) {
        for (var uid of users.keys()) {
          var user_stats = users.get(uid);
          this_server.users_cnn.registerUserStatistics(uid, user_stats);
        }
      }
    }
  );

  var timerIO = setInterval(function () {
    clearInterval(timerIO);

    // Database
    this_server.db = db;

    // Create a new Socket.io server
    this_server.io = socketio.listen(server);

    // Create a MongoDB storage object
    var mongoStore = new MongoStore({
      mongooseConnection: db.connection,
      collection: config.sessionCollection
    });

    // Chequear e inicializar estado del servidor
    var status = this_server.server_state.getServerStatus();

    if (status === 1) {
      console.log('Servidor inicializa en espera de concurso');
      this_server.server_state.esperarConcurso();
      this_server.createTimerWaitContest();

      // Limpiar las respuestas
      var Pregunta = mongoose.model('Pregunta');
      Pregunta.removeAnswers(function () {
      });
    } else {
      console.log('Determinar estado de concurso...');
      this_server.inicializarConcursoActivo();
    }

    this.timer_asistencia = setInterval(function () {
      let Pregunta = mongoose.model('Pregunta');
      Pregunta.getCantUsuariosActivados(function (err, cantActivados) {
        if (!err) {
          this_server.io.to('moderadores').emit('msg-update-users-contest',
            {
              'total_registrados': cantActivados,
              'total_conectados': this_server.users_cnn.totalConcunsantesSocketsConnected()
            });
        }
      });
    }, 10000);

    // Intercept Socket.io's handshake request
    this_server.io.use(function (socket, next) {
      console.log('Investigando.................');
      // Use the 'cookie-parser' module to parse the request cookies
      cookieParser(config.sessionSecret)(socket.request, {}, function (err) {
        // Get the session id from the request cookies
        var sessionId = socket.request.signedCookies ? socket.request.signedCookies[config.sessionKey] : undefined;
        if (!sessionId) return next(new Error('sessionId was not found in socket.request'), false);
        // Use the mongoStorage instance to get the Express session information
        mongoStore.get(sessionId, function (err, session) {
          if (err) return next(err, false);
          if (!session) return next(new Error('session was not found for ' + sessionId), false);
          // Set the Socket.io session information
          socket.request.session = session;
          // Use Passport to populate the user details
          passport.initialize()(socket.request, {}, function () {
            passport.session()(socket.request, {}, function () {
              if (socket.request.user) {
                next(null, true);
              } else {
                next(new Error('User is not authenticated'), false);
              }
            });
          });
        });
      });
    });

    // Add an event listener to the 'connection' event
    this_server.io.on('connection', function (socket) {

      var user = socket.request.user;

      setTimeout(function () {
        this_server.users_cnn.registerUserConnection(user, socket, this_server.io);
        console.log('Conectado.................');
      }, 1000);

      config.files.server.sockets.forEach(function (socketConfiguration) {
        require(path.resolve(socketConfiguration))(this_server.io, socket);
      });

      this_server.createServerListeners(socket);
    });
    //  }
  }, 1000);

  return server;
};

var User = mongoose.model('User');

SolaciServer.prototype.inicializarConcursoActivo = function () {
  var status = this.server_state.getServerStatus();
  console.log('Estado del concurso: ', status);

  if (status === 2) {
    this.startContestServer();
  } else if (status === 3) {
    this.server_state.revert();
    this.startNewQuestion();
  } else if (status === 4) {
    this.startModeratorQuestion();
  } else if (status === 5) {
    // msg-end-questions-concursante
    this.startEndQuestions();
  } else if (status === 6) {
    // msg-winner-contest-concursante
    this.startShowWinner();
  }
};

SolaciServer.prototype.createTimerWaitContest = function () {

  var this_server = this;

  var mfconcurso = moment(this.server_state.fecha_concurso);
  var mfactual;

  // Timer para refrescar el tiempo de descuento del concurso
  var timerTimer = setInterval(function () {
    console.log('Sincronizando Timer...');

    mfactual = moment();

    var dr = moment.range(mfactual, mfconcurso);
    var diff = dr.diff();

    this_server.io.to('en.concurso').emit('msg-update-timer-contest',
      {
        'fecha_concurso': mfconcurso.valueOf(),
        'fecha_actual': mfactual.valueOf(),
        'diferencia': diff
      });
  }, this_server.REFRESH_TIME);

  mfactual = moment();
  var dr = moment.range(mfactual, mfconcurso);
  var diff = dr.diff();

  var timerConcurso = setInterval(function () {
    console.log('Se inicia el concurso...');

    this_server.startContestServer();

    clearInterval(timerTimer);
    clearInterval(timerConcurso);
  }, diff);
};

SolaciServer.prototype.startContestServer = function () {
  // Solicitar datos a la base de datos y enviarlos
  var this_server = this;

  console.log('...inicializar concurso');
  this.server_state.iniciarConcurso();

  // Enviar a concursantes
  this.io.to('moderadores').emit('msg-start-contest-moderador', {
    'imagen_inicial': 'url/imagen.png',
    'descripcion': 'texto',
    'cantidad_preguntas': this_server.server_state.lista_preguntas.length
  });

  // Enviar a concursantes
  this.io.to('concursantes').emit('msg-start-contest-concursante', {
    'descripcion': 'texto'
  });
};

SolaciServer.prototype.startNewQuestion = function () {

  //var users_cnn = this.users_cnn;
  var this_server = this;

  console.log('...comenzar con una preguenta');
  this.server_state.realizarPregunta();

  var cant_preguntas = this_server.server_state.lista_preguntas.length;
  var cant_concursantes = this_server.users_cnn.maxConcunsantesSocketsConnected();
  var pregunta_actual = this_server.server_state.pregunta_activa;

  this_server.io.to('moderadores').emit('msg-update-statistics-contest-moderador', {
    'cant_usuarios_activados': cant_concursantes,
    'preguntas': cant_preguntas,
    'respuestas_correctas': 0,
    'respuestas_incorrectas': 0,
    'abstenciones': 0,
    'tiempo_total': 0,
    'tiempo_promedio': 0,
    'pregunta_actual': pregunta_actual
  });

  var question = this.server_state.next();
  var nquestion = this.server_state.current();

  if (question) {
    console.log('...se asigna una nueva pregunta y se envía a los interesados');
    var time = question.time * 1000;

    var opts = [];
    for (var opt of question.options) {
      opts.push({'_id': opt._id, 'name': opt.name});
    }

    var oawnser = {
      'qid': question._id,
      'qname': question.title,
      'options': opts,
      'time': time,
      'status': 0,
      'oid': ''
    };

    var oquestion = {
      'question': question,
      'next_question': nquestion,
      'time': time,
    };

    this.io.to('moderadores').emit('msg-start-question-moderador', oquestion);
    this.io.to('concursantes').emit('msg-start-question-concursante', oawnser);

    var mfpregunta = this.startTime = moment();
    this.server_state.setStartTime(this.startTime);

    this.endTime = moment(this.startTime.valueOf() + time);

    // Timer para refrescar el tiempo de descuento de la pregunta
    this.timer_tiempo = setInterval(function () {
      console.log('...sincronizando tiempo de pregunta');

      mfpregunta = moment();
      var dr = moment.range(mfpregunta, this_server.endTime);
      var diff = dr.diff();

      this_server.io.emit('msg-update-timer-question',
        {
          'fecha_actual': mfpregunta.valueOf(),
          'fecha_final': this_server.endTime.valueOf(),
          'diferencia': diff
        });
    }, this_server.REFRESH_TIME);

    var timerEstadistica = setInterval(function () {
      var Pregunta = mongoose.model('Pregunta');
      Pregunta.getEstadisticas(
        question._id,
        function (err, obj) {
          if (!err) {
            this_server.io.to('moderadores').emit('msg-update-statistics-question-moderador', {
              'cant_usuarios_activados': cant_concursantes,
              'preguntas': cant_preguntas,
              'respuestas_correctas': obj.respuestas_correctas,
              'respuestas_incorrectas': obj.respuestas_incorrectas,
              'abstenciones':  this_server.users_cnn.totalConcunsantesSocketsConnected() - (obj.respuestas_correctas + obj.respuestas_incorrectas) ,
              'tiempo_promedio': obj.tiempo_promedio,
              'question_id': question._id,
              'pregunta_actual': pregunta_actual
            });
          }
        });
    }, 5000);

    console.log('En espera de respuesta de pregunta por: ', Math.floor(time / 1000), 'segundos');
    this.timer_general = setInterval(function () {
      console.log('...tiempo de pregunta agotado y comenzar a moderar pregunta');
      if (timerEstadistica) {
        clearInterval(timerEstadistica);
      }

      this_server.startModeratorQuestion();
    }, time);
  } else {
    console.log('...no hay más preguntas a realizar');
    console.log('...enviando mensaje de fin de preguntas');
    this.startEndQuestions();
  }
};

SolaciServer.prototype.startModeratorQuestion = function () {
  var this_server = this;

  if (this.timer_general) {
    clearInterval(this.timer_general);
  }

  if (this.timer_tiempo) {
    clearInterval(this.timer_tiempo);
  }

  console.log('...inicializar moderación');
  this.server_state.moderarPregunta();


  var question = this.server_state.previus();

  var nquestion = this.server_state.current();

  var cant_preguntas = this_server.server_state.lista_preguntas.length;
  var cant_concursantes = this_server.users_cnn.maxConcunsantesSocketsConnected();
  var pregunta_actual = this_server.server_state.pregunta_activa;

  if (question) {
    var crtoption = null;

    question.options.forEach(function (opt, idx, arr) {
      if (opt.correct) {
        crtoption = opt;
        return false;
      }
    });

    if (this.users_cnn.concursantes_count > 0) {
      for (var kuser in this.users_cnn.concursantes) {

        var msg_std_awnser = {
          'qid': question._id,
          'qname': question.title,
          'option_correct': crtoption,
          'time_answered': -1,
          'option_answered': null
        };

        var muanswer = this.server_state.getRegisterAnswer(kuser);

        if (muanswer) {
          var opanwser = null;

          for (var opt of question.options) {
            if (opt._id.toString() === muanswer.oid.toString()) {
              opanwser = opt;
              break;
            }
          }

          msg_std_awnser.time_answered = muanswer.time;
          msg_std_awnser.option_answered = opanwser;
        }

        this.users_cnn.concursantes[kuser].sendMessage('msg-wait-moderation-concursante', msg_std_awnser);
      }
    }

    var opciones = this_server.server_state.getOptionStatistic();
    console.log(opciones);

    this.io.to('moderadores').emit('msg-wait-moderation-moderador', {
      'question': question,
      'next_question': nquestion,
      'opciones': opciones
    });

    var Pregunta = mongoose.model('Pregunta');
    Pregunta.getEstadisticas(
      question._id,
      function (err, obj) {
        if (!err) {
          this_server.io.to('moderadores').emit('msg-update-statistics-question-moderador', {
            'cant_usuarios_activados': cant_concursantes,
            'preguntas': cant_preguntas,
            'respuestas_correctas': obj.respuestas_correctas,
            'respuestas_incorrectas': obj.respuestas_incorrectas,
            'abstenciones':  this_server.users_cnn.totalConcunsantesSocketsConnected() - (obj.respuestas_correctas + obj.respuestas_incorrectas),
            'tiempo_promedio': obj.tiempo_promedio,
            'question_id': question._id,
            'pregunta_actual': pregunta_actual,
            'opciones': opciones
          });
        }
      });
  }
};

SolaciServer.prototype.startEndQuestions = function () {
  var this_server = this;

  console.log('...finalizar preguntas');
  this.server_state.finalizarPreguntas();

  var cant_preguntas = this_server.server_state.pregunta_activa;
  var cant_concursantes = this_server.users_cnn.maxConcunsantesSocketsConnected();

  var Pregunta = mongoose.model('Pregunta');

  Pregunta.getEstadisticasUsuarios(
    function (err, users) {
      if (!err) {
        for (var uid of users.keys()) {
          var duser = this_server.users_cnn.concursantes[uid];
          if (duser) {
            var user_stats = users.get(uid);
            duser.sendMessage('msg-end-questions-concursante', {
              'cant_usuarios_activados': cant_concursantes,
              'preguntas': cant_preguntas,
              'respuestas_correctas': user_stats.correct,
              'respuestas_incorrectas': user_stats.incorrect,
              'abstenciones': (cant_preguntas - (user_stats.correct + user_stats.incorrect)),
              'tiempo_total': user_stats.time,
              'tiempo_promedio': (user_stats.correct) ? (user_stats.time / user_stats.correct) : 0
            });
            this_server.users_cnn.registerUserStatistics(uid, user_stats);
          }
        }
      }
    }
  );

  Pregunta.getEstadisticasPreguntas(
    function (err_est, questions) {
      if (!err_est) {
        Pregunta.getWinner(function (err, user) {
          if (!err) {
            questions.forEach(function (value) {
              value.abstenciones = (cant_concursantes)?(cant_concursantes - value.abstenciones):0;
            });

            this_server.io.to('moderadores').emit('msg-update-statistics-contest-moderador', {
              'estadisticas_preguntas': questions
            });

            this_server.io.to('moderadores').emit('msg-end-questions-moderador', {
              'estadisticas_preguntas': questions,
              'user_win': user
            });
          }
        });
      }
    }
  );

  //Pregunta.getEstadisticasFinales(
  //  function (err_est, obj) {
  //    if (!err_est) {
  //      Pregunta.getWinner(function (err, user) {
  //        if (!err) {
  //          this_server.io.to('moderadores').emit('msg-update-statistics-contest-moderador', {
  //            'cant_usuarios_activados': cant_concursantes,
  //            'preguntas': cant_preguntas,
  //            'respuestas_correctas': obj.respuestas_correctas,
  //            'respuestas_incorrectas': obj.respuestas_incorrectas,
  //            'abstenciones': obj.abstenciones,
  //            'tiempo_promedio': obj.tiempo_promedio,
  //            'pregunta_actual': cant_preguntas
  //          });
  //
  //          this_server.io.to('moderadores').emit('msg-end-questions-moderador', {
  //            'cant_usuarios_activados': cant_concursantes,
  //            'preguntas': cant_preguntas,
  //            'respuestas_correctas': obj.respuestas_correctas,
  //            'respuestas_incorrectas': obj.respuestas_incorrectas,
  //            'abstenciones': obj.abstenciones,
  //            'tiempo_promedio': obj.tiempo_promedio,
  //            'pregunta_actual': cant_preguntas,
  //            'user_win': user
  //          });
  //        }
  //      });
  //    }
  //  }
  //);
};

SolaciServer.prototype.startShowWinner = function () {
  var this_server = this;

  console.log('...presentar ganador');
  this.server_state.presentarGanador();

  var cant_preguntas = this_server.server_state.pregunta_activa;
  var cant_concursantes = this_server.users_cnn.maxConcunsantesSocketsConnected();

  var Pregunta = mongoose.model('Pregunta');

  Pregunta.getWinner(
    function (err, user) {
      if (!err) {
        var winner_stats = this_server.users_cnn.getUserStatistics(user._id);
        if (winner_stats) {
          console.log('Total de preguntas:', cant_preguntas);
          this_server.io.to('moderadores').emit('msg-winner-contest-presentacion', {
            'user': user,
            'cant_usuarios_activados': cant_concursantes,
            'preguntas': cant_preguntas,
            'respuestas_correctas': winner_stats.stats.respuestas_correctas,
            'respuestas_incorrectas': winner_stats.stats.respuestas_incorrectas,
            'abstenciones': (cant_preguntas - (winner_stats.stats.respuestas_correctas + winner_stats.stats.respuestas_incorrectas)),
            //'abstenciones': (winner_stats.stats.abstenciones),
            'tiempo_total': winner_stats.stats.tiempo_total,
            'tiempo_promedio': winner_stats.stats.tiempo_promedio
          });
        }
        //Pregunta.getEstadisticasUsuario(
        //  user._id,
        //  function (err_stat, user_stat) {
        //    if (!err_stat) {
        //      this_server.io.to('moderadores').emit('msg-winner-contest-presentacion', {
        //        'user': user,
        //        'cant_usuarios_activados': cant_concursantes,
        //        'preguntas': cant_preguntas,
        //        'respuestas_correctas': user_stat.respuestas_correctas,
        //        'respuestas_incorrectas': user_stat.respuestas_incorrectas,
        //        'abstenciones': user_stat.abstenciones,
        //        'tiempo_promedio': user_stat.tiempo_promedio
        //      });
        //    }
        //  }
        //);
      }
    });
};

SolaciServer.prototype.startEndContest = function () {
  console.log('...finalizar concurso');
  this.server_state.finalizarConcurso();
  this.io.to('moderadores').emit('msg-end-contest-moderador', '');
  this.io.to('concursantes').emit('msg-end-contest-concursante', '');
};

SolaciServer.prototype.createServerListeners = function (socket) {
  //let users_cnn = this.users_cnn;
  let this_server = this;

  // Server events
  socket.on('msg-start-question-server', function (message) {
    console.log('Se solicita una nueva pregunta...');
    this_server.startNewQuestion();
  });

  // Server events
  socket.on('msg-end-questions', function (message) {
    console.log('Se solicita terminar ronda de preguntas...');
    this_server.startEndQuestions();
  });

  socket.on('msg-answer-question', function (message) {
    var status = this_server.server_state.getServerStatus();
    if (status === 3) {
      var fanswer = moment();
      var dr = moment.range(this_server.startTime, fanswer);
      var time = dr.diff();
      console.log('Respuesta en: ', Math.floor(time / 1000), 'segundos');

      if (!this_server.server_state.existUserAnswer(message.uid)) {
        this_server.server_state.registerAnswer(message.uid, message.qid, message.oid, time);


        var User = mongoose.model('User');
        User.findOne({_id: message.uid}, function (err, user) {
          if (!err) {
            // Salvar base de datos
            var Pregunta = mongoose.model('Pregunta');
            var fake = true;
           if(user.player){
              fake = false;
            }
            Pregunta.insertRespuesta(
                message.qid, {
                  option: message.oid,
                  user: message.uid,
                  time: time,
                  fake: fake
                },
                function (err, obj) {
                  if (err) {
                    console.log(err);
                  }
                });
          } else {
            console.log(err);
          }
        });
      }
    }
  });

  socket.on('msg-winner-contest', function (message) {
    this_server.startShowWinner();
  });

  socket.on('msg-end-contest', function (message) {
    this_server.startEndContest();
  });

  socket.on('msg-activated-user', function (message) {
    // emit msg-activated-user
    // console.log(message);
    var users_cnn = this_server.users_cnn;
    if (users_cnn.por_activar[message.uid]) {
      var User = mongoose.model('User');
      User.findOne({_id: message.uid}, function (err, user) {
        if (!err) {
          users_cnn.por_activar[message.uid].user = user;
          users_cnn.activateUserConnection(message.uid);
        } else {
          console.log(err);
        }
      });
    }
  });
};
