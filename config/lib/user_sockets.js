'use strict';

exports = module.exports = UserSockets;

function UserSockets(uid, user) {
  if(!(this instanceof UserSockets)) return new UserSockets();

  this.uid = uid;
  this.user = user;

  this.sockets = {}; // sid -> socket
  this.sockets_count = 0;
}

UserSockets.prototype.addSocket = function(socket) {
  var user = this;

  user.sockets[socket.id] = socket;
  user.sockets_count ++;

  return this;
};

UserSockets.prototype.removeSocket = function(socket) {
  var user = this;

  if(socket) {
    delete user.sockets[socket.id];
    user.sockets_count --;
  }

  return this;
};

UserSockets.prototype.count = function() {
  var user = this;

  return user.sockets_count;
};

UserSockets.prototype.sendMessage = function(msg, data) {
  var user = this;

  for(var sid in user.sockets) {
    user.sockets[sid].emit(msg, data);
  }
  return this;
};

UserSockets.prototype.joinRoom = function(room) {
  var user = this;

  for(var sid in user.sockets) {
    user.sockets[sid].join(room);
  }
  return this;
};
